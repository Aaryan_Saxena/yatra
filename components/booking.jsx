import React, { Component } from "react";
import "./navbar.css";
import "./booking.css";
import httpService from "./httpService";
import authService from "./authService";
class Booking extends Component {
  state = {
    tickets: {},
    promo: 0,
    cancellation: "",
    protection: "",
    adultInfo: [],
    infantInfo: [],
    childInfo: [],
    cntctDet: { email: "", mobile: "" },
    errors: {},
  };
  async fetchData() {
    let user = authService.getUser();
    let response = await httpService.get(`/bookingDetails`);
    if (user && user.id) {
      var userInfo = await httpService.get(`/getUser/${user.id}`);
      this.setState({
        cntctDet: { email: userInfo.data.email, mobile: userInfo.data.mobile },
      });
    }
    let adultInfo = [];
    let childInfo = [];
    let infantInfo = [];
    for (let i = 1; i <= response.data.members.adult; i++) {
      let adultDetail = { firstName: "", lastName: "" };
      adultInfo.push(adultDetail);
    }
    for (let i = 1; i <= response.data.members.child; i++) {
      let childDetail = { firstName: "", lastName: "" };
      childInfo.push(childDetail);
    }
    for (let i = 1; i <= response.data.members.infant; i++) {
      let infantDetail = { firstName: "", lastName: "" };
      infantInfo.push(infantDetail);
    }
    this.setState({
      tickets: response.data,
      adultInfo: adultInfo,
      childInfo: childInfo,
      infantInfo: infantInfo,
    });
  }
  handleDetails = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    let index = +input.name.substring(0, 1);
    if (
      input.name.substring(1) === "adultFirstName" ||
      input.name.substring(1) === "adultLastName"
    ) {
      let name =
        input.name.substring(6) === "FirstName" ? "firstName" : "lastName";
      s1.adultInfo[index][name] = input.value;
    }
    if (
      input.name.substring(1) === "childFirstName" ||
      input.name.substring(1) === "childLastName"
    ) {
      let name =
        input.name.substring(6) === "FirstName" ? "firstName" : "lastName";
      s1.childInfo[index][name] = input.value;
    }
    if (
      input.name.substring(1) === "infantFirstName" ||
      input.name.substring(1) === "infantLastName"
    ) {
      let name =
        input.name.substring(7) === "FirstName" ? "firstName" : "lastName";
      s1.infantInfo[index][name] = input.value;
    }
    this.setState(s1);
  };
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchData();
  }
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    if (input.type === "email" || input.type === "number") {
      s1.cntctDet[input.name] = input.value;
      this.handleValidate(e);
    }
    if (input.type === "checkbox" || input.type === "radio") {
      input.type === "checkbox"
        ? (s1[input.name] = input.checked)
        : (s1[input.name] = +input.value);
    }
    this.setState(s1);
  };
  handleValidate = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    let { email, mobile } = s1.cntctDet;
    switch (input.name) {
      case "email":
        s1.errors.email = this.handleValidateEmail(email);
        break;
      case "mobile":
        s1.errors.mobile = this.handleValidateMobile(mobile);
        break;
      default:
        break;
    }
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    let adulterrors = s1.adultInfo.reduce(
      (acc, curr) =>
        !curr.firstName || !curr.lastName ? (acc = acc + 1) : acc,
      0
    );
    let childerrors = s1.childInfo.reduce(
      (acc, curr) =>
        !curr.firstName || !curr.lastName ? (acc = acc + 1) : acc,
      0
    );
    let infanterrors = s1.infantInfo.reduce(
      (acc, curr) =>
        !curr.firstName || !curr.lastName ? (acc = acc + 1) : acc,
      0
    );
    if (adulterrors > 0 || childerrors > 0 || infanterrors > 0) {
      s1.errors.details = "Please fill all the travler details";
    } else if (!s1.cntctDet.email || !s1.cntctDet.mobile) {
      alert("Fill Contact Details");
    } else {
      let json = {
        goFlight: s1.tickets.goFlight,
        rtnFlight: s1.tickets.rtnFlight,
        totalFare:
          (s1.tickets.goFlight.Price +
            (s1.tickets.rtnFlight.Price ? s1.tickets.rtnFlight.Price : 0)) *
            (s1.tickets.members.adult +
              s1.tickets.members.child +
              s1.tickets.members.infant) +
          622 -
          s1.promo +
          (s1.cancellation ? 747 : 0) +
          (s1.protection
            ? (s1.tickets.members.adult +
                s1.tickets.members.child +
                s1.tickets.members.infant) *
              269
            : 0),
        cntctDet: s1.cntctDet,
        travellerInfo: {
          adult: s1.adultInfo,
          child: s1.childInfo,
          infant: s1.infantInfo,
        },
        members: s1.tickets.members,
      };
      this.handlePayment("/bookingSummary", json);
    }
    this.setState(s1);
  };
  async handlePayment(url, obj) {
    let response = await httpService.post(url, obj);
    this.props.history.push("/payment");
  }
  handleValidateEmail = (email) => {
    let format = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (format.test(email)) {
      return "";
    } else {
      return "Enter a Valid email";
    }
  };
  handleValidateMobile = (mobile) =>
    mobile
      ? mobile.length < 10
        ? "Mobile No must be of 10 Digit"
        : ""
      : "Mobile No is Mandatory";

  render() {
    let { goFlight = {}, members = {}, rtnFlight = {} } = this.state.tickets;
    let user = authService.getUser();
    let {
      promo = 0,
      cancellation,
      protection,
      childInfo = [],
      infantInfo = [],
      adultInfo = [],
      cntctDet = {},
      errors = {},
    } = this.state;
    let totTickets = members.adult + members.child + members.infant;
    let totalFare =
      (goFlight.Price + (rtnFlight.Price ? rtnFlight.Price : 0)) * totTickets +
      622 -
      promo +
      (cancellation ? 747 : 0) +
      (protection ? totTickets * 269 : 0);
    let addOn =
      (promo !== 0 ? 1 : 0) + (cancellation ? 1 : 0) + (protection ? 1 : 0);

    return (
      <div className="container-fluid bookingBackground">
        <div className="row mt-4 ml-1">
          <div className="col-9">
            <div className="row mt-2">
              <div className="col-1 text-left box-title">
                <i className="fa fa-search"></i>
              </div>
              <div className="col-lg-8 col-10 text-left box-title">
                {" "}
                Review your Bookings
              </div>
            </div>
            <div className="row bg-white" id="box2">
              <div className="col-12">
                <div className="row bg-white pt12">
                  <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                    <div className="row pl-1">
                      <div className="col-lg-12 col-12">
                        <img src={goFlight.logo} style={{ width: "40px" }} />
                      </div>
                    </div>
                    <div className="row pl-1">
                      <div className="col-lg-12 col-12">{goFlight.name}</div>
                    </div>
                    <div className="row pl-1 text-secondary">
                      <div className="col-lg-12 col-12"> {goFlight.airBus}</div>
                    </div>
                  </div>
                  <div className="col-lg-2 col-4">
                    <div className="row fs-base">{goFlight.desDept}</div>
                    <div className="row lh28 gray-dark fs-24 bold">
                      {goFlight.timeDept}
                    </div>
                    <div className="row text-secondary fs11">{goFlight.T1}</div>
                  </div>
                  <div className="col-lg-6 col-3">
                    <div className="row">
                      <div className="col-12 text-center no-padding-margin">
                        {goFlight.total}
                        <span className="gray-lightest">|</span>
                        <span className=""> {goFlight.meal} Meal </span>
                        <span className="gray-lightest">|</span>
                        <span className="">{members.class}</span>
                      </div>
                    </div>
                    <div className="row two-dots">
                      <div className="col-4 no-padding-margin">
                        <hr />
                      </div>
                      <div className="col-lg-4 text-center no-padding-margin">
                        <span className="type-text">
                          <i className="fa fa-fighter-jet" id="ic1"></i>Flight
                        </span>
                      </div>
                      <div className="col-4 no-padding-margin">
                        <hr />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col text-center">
                        25 kgs |{" "}
                        <span className="text-success">
                          Partially Refundable
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="col-lg-2 col-5">
                    <div className="row fs-base">{goFlight.desArr}</div>
                    <div className="row lh28 gray-dark fs-24 bold">
                      {goFlight.timeArr}
                    </div>
                    <div className="row text-secondary fs11">{goFlight.T2}</div>
                  </div>
                </div>
                {rtnFlight.id && (
                  <React.Fragment>
                    <div className="row bg-white">
                      <div className="col-2 d-none d-lg-block"></div>
                      <div className="col-lg-10 col-12 rtn-flight"></div>
                    </div>
                    <div className="row bg-white pt12">
                      <div className="col-lg-2 col-12 text-center d-none d-lg-block">
                        <div className="row pl-1">
                          <div className="col-lg-12 col-12">
                            <img
                              src={rtnFlight.logo}
                              style={{ width: "40px" }}
                            />
                          </div>
                        </div>
                        <div className="row pl-1">
                          <div className="col-lg-12 col-12">
                            {rtnFlight.name}
                          </div>
                        </div>
                        <div className="row pl-1 text-secondary">
                          <div className="col-lg-12 col-12">
                            {" "}
                            {rtnFlight.airBus}
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-2 col-4">
                        <div className="row fs-base">{rtnFlight.desDept}</div>
                        <div className="row lh28 gray-dark fs-24 bold">
                          {rtnFlight.timeDept}
                        </div>
                        <div className="row text-secondary fs11">
                          {rtnFlight.T1}
                        </div>
                      </div>
                      <div className="col-lg-6 col-3">
                        <div className="row">
                          <div className="col-12 text-center no-padding-margin">
                            {rtnFlight.total}
                            <span className="gray-lightest">|</span>
                            <span className=""> {rtnFlight.meal} Meal </span>
                            <span className="gray-lightest">|</span>
                            <span className="">{members.class}</span>
                          </div>
                        </div>
                        <div className="row two-dots">
                          <div className="col-4 no-padding-margin">
                            <hr />
                          </div>
                          <div className="col-lg-4 text-center no-padding-margin">
                            <span className="type-text">
                              <i className="fa fa-fighter-jet" id="ic1"></i>
                              Flight
                            </span>
                          </div>
                          <div className="col-4 no-padding-margin">
                            <hr />
                          </div>
                        </div>
                        <div className="row">
                          <div className="col text-center">
                            25 kgs |{" "}
                            <span className="text-success">
                              Partially Refundable
                            </span>
                          </div>
                        </div>
                      </div>
                      <div className="col-lg-2 col-5">
                        <div className="row fs-base">{rtnFlight.desArr}</div>
                        <div className="row lh28 gray-dark fs-24 bold">
                          {rtnFlight.timeArr}
                        </div>
                        <div className="row text-secondary fs11">
                          {rtnFlight.T2}
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                )}
                <div className="note-block-new full-lrb">
                  <i className="ytfi-info-circled"></i>
                  <span className="bold fs-sm ng-binding">
                    Compulsory Guidelines for Passengers
                  </span>
                  <ul>
                    <li>
                      <span className="fs-sm ">
                        You need to certify your health status through Aarogya
                        Setu app preactivated on your mobile or self-declaration
                        form.
                        <a
                          href="#"
                          ng-show="msg.link"
                          target="_blank"
                          className="text-decoration-none"
                        >
                          click here
                        </a>
                      </span>
                    </li>
                    <li>
                      <span className="fs-sm">
                        Face Mask is mandatory both at the airport &amp; in
                        Flight.
                        <a
                          href="#"
                          ng-show="msg.link"
                          target="_blank"
                          className="text-decoration-none"
                        >
                          click here
                        </a>
                      </span>
                    </li>
                    <li>
                      <span className="fs-sm">
                        Failure to comply with Covid-19 protocols and the
                        directions of ground staff and/or crew may attract penal
                        action against the concerned individual.
                        <a
                          href="#"
                          ng-show="msg.link"
                          target="_blank"
                          className="text-decoration-none"
                        >
                          click here
                        </a>
                      </span>
                    </li>
                    <li>
                      <span className="fs-sm">
                        Only passengers with confirmed web check-in will be
                        allowed to enter 2 hours prior to the Flight departure.
                        <a
                          href="#"
                          ng-show="msg.link"
                          target="_blank"
                          className="text-decoration-none"
                        >
                          click here
                        </a>
                      </span>
                    </li>
                    <li>
                      <span className="fs-sm">
                        Only one check-in bag and cabin bag will be allowed per
                        customer with a baggage tag affixed on the bag.
                        <a
                          href=""
                          ng-show="msg.link"
                          target="_blank"
                          className="text-decoration-none"
                        >
                          click here
                        </a>
                      </span>
                    </li>
                    <li>
                      <span className="fs-sm">
                        View all mandatory travel guidelines issued by Govt. of
                        India here
                        <a
                          href="https://bit.ly/3gjVF7o"
                          ng-show="msg.link"
                          target="_blank"
                          className="text-decoration-none"
                        >
                          click here
                        </a>
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <br />
            <div className="row bg-white" id="box2">
              <div className="col-12">
                <div className="row bg-white">
                  <div className="col-1 text-right"></div>
                </div>
                <div className="row bg-white">
                  <div className="col-lg-1 col-2 text-right">
                    <input
                      className="form-check-input ng-untouched ng-pristine ng-valid"
                      name="cancellation"
                      type="checkbox"
                      onChange={this.handleChange}
                      checked={cancellation}
                    />
                  </div>
                  <div class="col-1">
                    <svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="plane-departure"
                      className="svg-inline--fa fa-plane-departure fa-w-20 "
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 640 512"
                      style={{ color: "green" }}
                    >
                      <path
                        fill="currentColor"
                        d="M624 448H16c-8.84 0-16 7.16-16 16v32c0 8.84 7.16 16 16 16h608c8.84 0 16-7.16 16-16v-32c0-8.84-7.16-16-16-16zM80.55 341.27c6.28 6.84 15.1 10.72 24.33 10.71l130.54-.18a65.62 65.62 0 0 0 29.64-7.12l290.96-147.65c26.74-13.57 50.71-32.94 67.02-58.31 18.31-28.48 20.3-49.09 13.07-63.65-7.21-14.57-24.74-25.27-58.25-27.45-29.85-1.94-59.54 5.92-86.28 19.48l-98.51 49.99-218.7-82.06a17.799 17.799 0 0 0-18-1.11L90.62 67.29c-10.67 5.41-13.25 19.65-5.17 28.53l156.22 98.1-103.21 52.38-72.35-36.47a17.804 17.804 0 0 0-16.07.02L9.91 230.22c-10.44 5.3-13.19 19.12-5.57 28.08l76.21 82.97z"
                      ></path>
                    </svg>
                  </div>
                </div>
                <div className="row text-success fs10">
                  <div className="col ml-1 h5">Cancellation Policy</div>
                </div>
                <div className="row fs14">
                  <div className="col ml-1">
                    Zero cancellation fee for your tickets when you cancel. Pay
                    additional Rs.747
                  </div>
                </div>
                <div className="row bg-white">
                  <div
                    className="col-12 text-center d-none d-lg-block"
                    id="fs14"
                    style={{ backgroundColor: "rgb(255, 252, 199)" }}
                  >
                    {" "}
                    Travel Smart: Get additional refund of Rs.3,051 in case of
                    cancellation. Terms &amp; Conditions{" "}
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-1 text-right box-title">
                <svg
                  aria-hidden="true"
                  focusable="false"
                  data-prefix="fas"
                  data-icon="user-edit"
                  className="svg-inline--fa fa-user-edit fa-w-20 userEdit"
                  role="img"
                  xmlns="http://www.w3.org/2000/svg"
                  viewBox="0 0 640 512"
                >
                  <path
                    fill="currentColor"
                    d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h274.9c-2.4-6.8-3.4-14-2.6-21.3l6.8-60.9 1.2-11.1 7.9-7.9 77.3-77.3c-24.5-27.7-60-45.5-99.9-45.5zm45.3 145.3l-6.8 61c-1.1 10.2 7.5 18.8 17.6 17.6l60.9-6.8 137.9-137.9-71.7-71.7-137.9 137.8zM633 268.9L595.1 231c-9.3-9.3-24.5-9.3-33.8 0l-37.8 37.8-4.1 4.1 71.8 71.7 41.8-41.8c9.3-9.4 9.3-24.5 0-33.9z"
                  ></path>
                </svg>
              </div>
              <div className="col-11 box-title">
                {" "}
                Enter Traveller Details |{" "}
                <span className="fs-18">
                  Sign in to book faster and use eCash{" "}
                </span>
              </div>
            </div>
            <div className="row bg-white" id="box2">
              <div className="col-12">
                <div className="row bg-white ">
                  <div className="col-lg-2 col-4" id="fs14">
                    <b>Contact :</b>
                  </div>
                  <div className="row">
                    <div className="col-6 form-group">
                      <input
                        className="form-control"
                        name="email"
                        placeholder="Enter email"
                        type="email"
                        value={cntctDet.email}
                        onChange={this.handleChange}
                        required={!cntctDet.email}
                        disabled={user && user.id}
                      />
                      {errors.email && (
                        <span className="text-danger ml-2">{errors.email}</span>
                      )}
                    </div>
                    <div className="col-6 form-group">
                      <input
                        className="form-control"
                        placeholder="Mobile Number"
                        type="number"
                        name="mobile"
                        value={cntctDet.mobile}
                        onChange={this.handleChange}
                        disabled={user && user.id}
                      />
                      {errors.mobile && (
                        <span className="text-danger ml-2">
                          {errors.mobile}
                        </span>
                      )}
                    </div>
                  </div>
                </div>
                <div className="row bg-white box3row2">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12 fs14">
                    {" "}
                    Your booking details will be sent to this email address and
                    mobile number.{" "}
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12 fs14">
                    {" "}
                    Also send my booking details on WhatsApp{" "}
                    <i
                      className="fab fa-whatsapp-square"
                      style={{ color: "green", fontSize: "18px" }}
                    ></i>
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12">
                    <hr />
                  </div>
                </div>
                <div class="row bg-white">
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-10 fs14">
                    <b>Traveller Information</b>
                  </div>
                </div>
                <div className="row bg-white" style={{ fontSize: "1rem" }}>
                  <div className="col-2 d-none d-lg-block"></div>
                  <div className="col-lg-10 col-12 fs14">
                    <b>Important Note:</b> Please ensure that the names of the
                    passengers on the travel documents is the same as on their
                    government issued identity proof.{" "}
                  </div>
                </div>
                <div className="text-danger text-center">{errors.details}</div>
                {adultInfo.map((obj, index) => (
                  <div className="row bg-white" key={index}>
                    <div className="col-2 fs-14 pb-0">Adult {index + 1} :</div>
                    {this.makeTextField(
                      index + "adultFirstName",
                      obj.firstName,
                      "First Name"
                    )}
                    {this.makeTextField(
                      index + "adultLastName",
                      obj.lastName,
                      "Last Name"
                    )}
                  </div>
                ))}
                {childInfo.map((obj, index) => (
                  <div className="row bg-white" key={index}>
                    <div className="col-2 fs-14">Child {index + 1} :</div>
                    {this.makeTextField(
                      index + "childFirstName",
                      obj.firstName,
                      "First Name"
                    )}
                    {this.makeTextField(
                      index + "childLastName",
                      obj.lastName,
                      "Last Name"
                    )}
                  </div>
                ))}
                {infantInfo.map((obj, index) => (
                  <div className="row bg-white" key={index}>
                    <div className="col-2 fs-14">Infant {index + 1} :</div>
                    {this.makeTextField(
                      index + "infantFirstName",
                      obj.firstName,
                      "First Name"
                    )}
                    {this.makeTextField(
                      index + "infantLastName",
                      obj.lastName,
                      "Last Name"
                    )}
                  </div>
                ))}
              </div>
            </div>
            <div className="row bg-white mt-2" id="box2">
              <div className="col-12 d-none d-lg-block">
                <div className="row bg-white box3row2">
                  <div className="col-1 text-center mt-1">
                    <i
                      className="fa fa-university"
                      style={{ fontSize: "35px" }}
                    ></i>
                  </div>
                  <div className="col-10 text-left">
                    <div className="row fs14">
                      <b>Add your GST Details (Optional)</b>
                    </div>
                    <div className="row fs14">
                      {" "}
                      Claim credit of GST charges. Your taxes may get updated
                      post submitting your GST details.{" "}
                    </div>
                  </div>
                </div>
                <div className="row bg-white box3row2">
                  <div className="col-1 text-center">
                    <svg
                      aria-hidden="true"
                      focusable="false"
                      data-prefix="fas"
                      data-icon="suitcase-rolling"
                      className="svg-inline--fa fa-suitcase-rolling fa-w-12 "
                      role="img"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 384 512"
                      style={{ fontSize: "35px" }}
                    >
                      <path
                        fill="currentColor"
                        d="M336 160H48c-26.51 0-48 21.49-48 48v224c0 26.51 21.49 48 48 48h16v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h128v16c0 8.84 7.16 16 16 16h32c8.84 0 16-7.16 16-16v-16h16c26.51 0 48-21.49 48-48V208c0-26.51-21.49-48-48-48zm-16 216c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zm0-96c0 4.42-3.58 8-8 8H72c-4.42 0-8-3.58-8-8v-16c0-4.42 3.58-8 8-8h240c4.42 0 8 3.58 8 8v16zM144 48h96v80h48V48c0-26.51-21.49-48-48-48h-96c-26.51 0-48 21.49-48 48v80h48V48z"
                      ></path>
                    </svg>
                  </div>
                  <div className="col-10 text-left">
                    <div className="row fs14">
                      <b>Travelling for work?</b>
                    </div>
                    <div className="row fs14">
                      {" "}
                      Join Yatra for Business. View Benefits{" "}
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="row mt-2">
              <div className="col-1 text-right userEdit">
                <i className="fa fa-umbrella"></i>
              </div>
              <div className="col-11 fs10">
                {" "}
                Travel Protection<span className="fs14">(Recommended)</span>
              </div>
            </div>
            <div className="row bg-white" id="box2">
              <div className="col-12">
                <div className="row bg-white">
                  <div className="col-2 text-center">
                    <input
                      className="form-check-input ng-untouched ng-pristine ng-valid"
                      name="protection"
                      type="checkbox"
                      onChange={this.handleChange}
                      checked={protection}
                    />
                  </div>
                  <div className="col-10 mt-1 fs14">
                    {" "}
                    Yes, Add Travel Protection to protect my trip (Rs.269 per
                    traveller){" "}
                  </div>
                </div>
                <div
                  className="row bg-white"
                  style={{ olor: "rgb(219, 154, 0)" }}
                >
                  <div className="col-1 d-none d-lg-block"></div>
                  <div className="col-11 d-none d-lg-block" id="fs14">
                    {" "}
                    6000+ travellers on Yatra protect their trip daily.
                    <span className="text-primary">Learn More</span>
                  </div>
                </div>
                <div className="row bg-white">
                  <div
                    className="col-12 text-left text-muted d-none d-lg-block"
                    id="fs14"
                  >
                    {" "}
                    Cover Includes:{" "}
                  </div>
                </div>
                <div className="row bg-white">
                  <div
                    className="col-4 text-center d-none d-lg-block"
                    id="fs14"
                  >
                    <div className="row">
                      <div className="col text-left">
                        <span className="fa-stack fa-2x">
                          <i className="fa fa-circle fa-stack-2x"></i>
                          <i className="fas fa-plane-departure fa-stack-1x fa-inverse"></i>
                        </span>
                      </div>
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      {" "}
                      Trip Cancellation{" "}
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      {" "}
                      Claim upto Rs.25,000{" "}
                    </div>
                  </div>
                  <div
                    className="col-4 text-center d-none d-lg-block"
                    style={{ fontSize: "1rem" }}
                  >
                    <div className="row mb-1">
                      <div className="col text-left">
                        <span className="fa-stack fa-2x">
                          <i className="fa fa-circle fa-stack-2x"></i>
                          <i class="fas fa-suitcase-rolling fa-stack-1x fa-inverse"></i>
                        </span>
                      </div>
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      {" "}
                      Loss of Baggage{" "}
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      {" "}
                      Claim upto Rs.25,000{" "}
                    </div>
                  </div>
                  <div
                    className="col-4 text-center d-none d-lg-block"
                    style={{
                      fontSize: "0.87rem",
                      fontFamily: "Rubik-Regular, Arial, Helvetica, sans-serif",
                    }}
                  >
                    <div className="row">
                      <div className="col text-left">
                        <span className="fa-stack fa-2x">
                          <i className="fa fa-circle fa-stack-2x"></i>
                          <i className="fa fa-ambulance fa-stack-1x fa-inverse"></i>
                        </span>
                      </div>
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      {" "}
                      Medical Emergency{" "}
                    </div>
                    <div className="row" style={{ fontSize: "0.87rem" }}>
                      {" "}
                      Claim upto Rs.25,000{" "}
                    </div>
                  </div>
                </div>
                <div className="row bg-white">
                  <div
                    className="col-12 text-center d-none d-lg-block"
                    style={{ fontSize: "0.87rem" }}
                  >
                    {" "}
                    Note: Travel Protection is applicable only for Indian
                    citizens below the age of 70 years.{" "}
                    <span className="text-primary">Terms &amp; Conditions</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-3">
            <div className="row mt-2 fs-md">
              <div className="col"> Fare Details </div>
            </div>
            <div className="row mt-2 bg-white" id="box3">
              <div className="col-12">
                <div className="row bg-white">
                  <div className="col-8 text-left fs11">
                    {" "}
                    Base Fare({totTickets} Traveller){" "}
                  </div>
                  <div className="col-4 text-right fs11">
                    {" "}
                    ₹ {goFlight.Price + (rtnFlight.Price ? rtnFlight.Price : 0)}
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-8 text-left" id="fs11">
                    {" "}
                    Fees &amp; Surcharge{" "}
                  </div>
                  <div className="col-4 text-right fs11"> ₹ 622</div>
                </div>
                <div className="row bg-white">
                  <div className="col-12 text-center">
                    <hr />
                  </div>
                </div>
                <div className="row bg-white">
                  <div className="col-6 text-left fs12"> Total Fare </div>
                  <div className="col-6 text-right fs12"> ₹ {totalFare}</div>
                </div>
                {promo !== 0 || cancellation || protection ? (
                  <div class="row bg-white">
                    <div class="col-6 text-left fs12">Add Ons({addOn})</div>
                  </div>
                ) : (
                  ""
                )}
              </div>
            </div>
            <div class="row" id="box4">
              <div class="col-7 text-left fs13">You Pay</div>
              <div class="col-5 text-right fs13">₹ {totalFare}</div>
            </div>
            <div className="row mt-4">
              <div className="col">Promo</div>
            </div>
            <div className="row bg-white ml-1" id="box3">
              <div className="col-12">
                <div className="row">
                  <div className="col-12"> Select A Promo Code </div>
                </div>
                {promo !== 0 && (
                  <div class="row">
                    <div class="col-1">
                      <i class="fas fa-check-circle promo-applied-icon"></i>
                    </div>
                    <div class="col-10">
                      <span class="fs-sm">
                        Promo applied successfully. You got a discount of Rs.
                        {promo}.
                      </span>
                    </div>
                  </div>
                )}
                <div className="row">
                  <div className="col-12">
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input"
                        type="radio"
                        name="promo"
                        value={400}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;NEWPAY</span>
                      </label>
                      <div className="row">
                        <div className="col-10 promoDesc">
                          Pay with PayPal to save upto Rs.1400 on Domestic
                          Flights (Max. discount Rs. 600 + 50% cashback up to
                          Rs. 800).
                        </div>
                      </div>
                    </div>
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input "
                        type="radio"
                        name="promo"
                        value={1000}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;YTAMZ19</span>
                      </label>
                      <div className="row">
                        <div className="col-10 promoDesc">
                          Save up to Rs.2,000 (Flat 6% (max Rs. 1,000) instant
                          OFF + Flat 5% (max Rs. 1,000) cashback).
                        </div>
                      </div>
                    </div>
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input "
                        type="radio"
                        name="promo"
                        value={900}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;YATAIR</span>
                      </label>
                      <div className="row">
                        <div className="col-10" className="col-10 promoDesc">
                          Flat 9% OFF. Valid with all cards and wallets.
                        </div>
                      </div>
                    </div>
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input "
                        type="radio"
                        name="promo"
                        value={1500}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;FREEFLY</span>
                      </label>
                      <div className="row">
                        <div className="col-10 promoDesc">
                          Up to 3700 instant off, applicable on all payment
                          modes.
                        </div>
                      </div>
                      ..
                    </div>
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input "
                        type="radio"
                        name="promo"
                        value={400}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;FLYFESTIVE</span>
                      </label>
                      <div className="row">
                        <div className="col-10 promoDesc">
                          Flat Rs.400 OFF per pax (up to Rs 2,400).
                        </div>
                      </div>
                    </div>
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input "
                        type="radio"
                        name="promo"
                        value={1000}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;YTPZ</span>
                      </label>
                      <div className="row">
                        <div className="col-10 promoDesc">
                          Flat 10% Instant OFF upto Rs 1000 on using PAYZAPP
                          Wallet.
                        </div>
                      </div>
                    </div>
                    <div className="form-check ml-3">
                      <input
                        className="form-check-input "
                        type="radio"
                        name="promo"
                        value={515}
                        onChange={this.handleChange}
                      />
                      <label className="form-check-label" id="promobox">
                        <span className="promoHead"> &nbsp;FLASHUPI</span>
                      </label>
                      <div className="row">
                        <div className="col-10 promoDesc">
                          Flat Rs.515 OFF per pax. Valid only with UPI Payment
                          modes.
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-12 text-center">
            <button
              className="btn btn-danger text-white"
              type="submit"
              style={{ borderRadius: "2px" }}
              onClick={this.handleSubmit}
            >
              Proceed to Payment
            </button>{" "}
          </div>
        </div>
      </div>
    );
  }
  makeTextField = (name, value, placeholder) => {
    return (
      <div className="col-5">
        <input
          type="text"
          className="form-control"
          name={name}
          value={value}
          placeholder={placeholder}
          onChange={this.handleDetails}
        />
      </div>
    );
  };
}
export default Booking;
