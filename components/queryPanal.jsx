import React, { Component } from "react";
class QueryPanal extends Component {
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let options = { ...this.props.options };
    input.type === "checkbox"
      ? (options[input.name] = this.updateCBs(
          options[input.name],
          input.checked,
          input.value
        ))
      : (options[input.name] = input.value);
    this.props.onOptionChange(options);
  };
  updateCBs = (inpValue, checked, value) => {
    let inpArr = inpValue ? inpValue.split(",") : [];
    if (checked) inpArr.push(value);
    else {
      let index = inpArr.findIndex((val) => val === value);
      if (index >= 0) inpArr.splice(index, 1);
    }
    return inpArr.join(",");
  };

  makeCBs = (arr, values, name, label) => {
    return (
      <div className="row bg-white">
        <div className="col-12">
          <label className="form-check-label">
            <span>{label}</span>
          </label>
        </div>
        <div>
          {arr.map((opt, index) => (
            <div className="col-12" key={index}>
              <div className="form-check">
                <input
                  type="checkbox"
                  className="form-check-input"
                  value={opt}
                  name={name}
                  checked={values.find((val) => val === opt) || false}
                  onChange={this.handleChange}
                />
                <label className="form-check-label">{opt}</label>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  };
  makeRadios = (arr, values, name, label) => {
    return (
      <div className="row bg-white">
        <div className="col-12">
          <label className="form-check-label">
            <span className="">{label}</span>
          </label>
        </div>
        <div>
          {arr.map((opt, index) => (
            <div className="col-12" key={index}>
              <div className="form-check">
                <input
                  type="radio"
                  className="form-check-input"
                  value={opt}
                  name={name}
                  checked={values === opt || false}
                  onChange={this.handleChange}
                />
                <label className="form-check-label">{opt}</label>
              </div>
            </div>
          ))}
        </div>
      </div>
    );
  };
  render() {
    let {
      price = "",
      time = "",
      flight = "",
      airbus = "",
    } = this.props.options;
    let { priceOpt, timeOpt, airlineOpt, aircraftOpt } = this.props;
    return (
      <React.Fragment>
        <div className="col-3">
          {this.makeRadios(priceOpt, price, "price", "Price")}
        </div>
        <div className="col-3">
          {this.makeRadios(timeOpt, time, "time", "Time")}
        </div>
        <div className="col-3">
          {this.makeCBs(airlineOpt, flight.split(","), "flight", "Airline")}
        </div>
        <div className="col-3">
          {this.makeCBs(aircraftOpt, airbus.split(","), "airbus", "Aircraft")}
        </div>
      </React.Fragment>
    );
  }
}
export default QueryPanal;
