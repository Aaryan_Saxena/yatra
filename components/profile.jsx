import React, { Component } from "react";
import httpService from "./httpService";
import { Link } from "react-router-dom";
import authService from "./authService";
import LeftPanel from "./leftpanel";
import "./navbar.css";
class Profile extends Component {
  state = {
    booking: [],
    user: {
      fname: "",
      lname: "",
      title: "",
      city: "",
      pincode: "",
      state: "",
      mobile: "",
      country: "",
      email: "",
      address: "",
      password: "",
    },
    view: 0,
    states: [
      "Andhra Pradesh",
      "Arunachal Pradesh",
      "Assam",
      "Bihar",
      "Chhattisgarh",
      "Goa",
      "Gujarat",
      "Haryana",
      "Himachal Pradesh",
      "Jharkhand",
      "Karnataka",
      "Kerala",
      "Madhya Pradesh",
      "Maharashtra",
      "Manipur",
      "Meghalaya",
      "Mizoram",
      "Nagaland",
      "Odisha",
      "Punjab",
      "Rajasthan",
      "Sikkim",
      "Tamil Nadu",
      "Telangana",
      "Tripura",
      "Uttar Pradesh",
      "Uttarakhand",
      "West Bengal",
    ],
  };
  async componentDidMount() {
    let user = authService.getUser();
    let userDetails = await httpService.get(`/getUser/${user.id}`);
    let response = await httpService.get("/allBookings");
    console.log(response);
    this.setState({ booking: response.data, user: userDetails.data });
  }
  async handleView(view) {
    let user = authService.getUser();
    let userDetails = await httpService.get(`/getUser/${user.id}`);
    this.setState({ view: view, user: userDetails.data });
  }
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.user[input.name] = input.value;
    this.setState(s1);
  };
  handleSubmit = (e) => {
    e.preventDefault();
    let user = authService.getUser();
    this.postData(`/editUser/${user.id}`, this.state.user);
  };
  async postData(url, obj) {
    let response = await httpService.post(url, obj);
    alert("Your profile has been updated successfully.");
  }
  render() {
    let { booking = [], user = {}, view, states } = this.state;
    let {
      fname = "",
      lname = "",
      title = "",
      city = "",
      pincode = "",
      state = "",
      mobile = "",
      country = "",
      email = "",
      address = "",
      password = "",
    } = user;
    return (
      <div className="container-fluid" style={{ minWidth: "978px" }}>
        <div className="row">
          <div className="col-9">
            <div className="row mt-3 mb-2">
              <div className="col-9 ml-1">
                Dashboard /{" "}
                <Link to="/yatra/profile" className="text-decoration-none">
                  <span>Your Profile</span>
                </Link>{" "}
              </div>
            </div>
            <div className="row ml-1" id="payment">
              <LeftPanel option={"Your Profile"} />
              <div className="col-9">
                <div className="row bg-light">
                  <div className="ml-4 mt-2 mb-2">MY ACCOUNT</div>
                </div>
                {view === 0 ? (
                  <React.Fragment>
                    <div className="row">
                      <div className="ml-3 mt-1 mb-1">MY PROFILE</div>
                    </div>
                    <div className="ml-1 mt-1 mr-1 row border mb-4">
                      <div className="mt-4 ml-1 text-center col-1 p-0">
                        <i className="fa fa-user-circle user-logo"></i>
                      </div>
                      <div className="mt-4 col-2 p-0">
                        <b>
                          Mr.&nbsp;{user.fname}&nbsp;{user.lname}
                        </b>
                      </div>
                      <div className="col-8 text-right mt-4">
                        <i
                          className="fa fa-pen-square sq-pen"
                          onClick={() => this.handleView(1)}
                        ></i>
                      </div>
                      <div className="col-12 p-0 ml-50">
                        <i className="fa fa-envelope"></i>&nbsp;&nbsp;
                        {user.email}
                      </div>
                      <div className="col-12"></div>{" "}
                      <div className="ml-50">
                        <i className="fa fa-phone"></i>&nbsp;&nbsp;{user.mobile}
                      </div>{" "}
                      <div className="col-12"></div>{" "}
                      <div className="ml-50">
                        <i className="fa fa-map-marker"></i>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{user.country}
                      </div>{" "}
                    </div>
                    <div className="row">
                      <div className="ml-3 mt-1 mb-1">GST DETAILS</div>
                    </div>
                    <div className="ml-1 mt-1 mr-1 row border mb-4">
                      <div className="mt-4 ml-3">
                        <i className="fas fa-money-check-alt user-logo"></i>
                      </div>
                      <div className="mt-4 ml-2">
                        <b>GST Number</b>
                      </div>
                      <div className="col-9 text-right mt-4">
                        <i className="fa fa-plus-square user-logo"></i>
                      </div>
                      <div className="ml-50">Add GST Details</div>
                    </div>
                    <div className="row">
                      <div className="ml-3 mt-1 mb-1">SAVED TRAVELLERS</div>
                    </div>
                    <div className="ml-1 mt-1 mr-1 row border mb-4">
                      <div className="mt-4 ml-3">
                        <i className="fa fa-users user-logo"></i>
                      </div>
                      <div className="mt-4 ml-2">
                        <b>No Travellers added</b>
                      </div>
                      <div className="col-8 ml-1 text-right mt-4">
                        <i className="fa fa-plus-square sq-pen"></i>
                      </div>
                      <div className="ml-50">
                        Add Traveller for a faster booking experience.
                      </div>
                    </div>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <div className="row">
                      <div className="ml-3 mt-1 mb-1">EDIT PROFILE</div>
                    </div>
                    <div className="ml-1 mt-1 mr-1 row border mb-4">
                      <form>
                        <div className="form-group required">
                          <div className="row mb-3 ml-4 mt-3">
                            <label className="control-label">User Name:</label>
                            <div className="row ml-2">
                              <div className="col-3">
                                <select
                                  className="browser-default custom-select"
                                  name="title"
                                  value={title}
                                  onChange={this.handleChange}
                                >
                                  <option>Title</option>
                                  <option>Mr.</option>
                                  <option>Ms.</option>
                                  <option>Mrs.</option>
                                  <option>Dr.</option>
                                </select>
                              </div>
                              <div className="col-4">
                                <input
                                  className="form-control"
                                  placeholder="First Name"
                                  name="fname"
                                  type="text"
                                  value={fname}
                                  onChange={this.handleChange}
                                />
                              </div>
                              <div className="col-4">
                                <input
                                  className="form-control"
                                  placeholder="Last Name"
                                  name="lname"
                                  id="lname"
                                  type="text"
                                  value={lname}
                                  onChange={this.handleChange}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="form-group row ml-3">
                          <label
                            for="email"
                            className="col-sm-2 col-form-label"
                          >
                            Email:
                          </label>
                          <div className="col-sm-10">
                            <input
                              className="form-control"
                              placeholder="Enter your Email"
                              name="email"
                              type="text"
                              disabled={email}
                              value={email}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div className="form-group row ml-3">
                          <label
                            for="password"
                            className="col-sm-2 col-form-label"
                          >
                            Password:
                          </label>
                          <div className="col-sm-10">
                            <input
                              className="form-control"
                              placeholder="Enter your Password"
                              name="password"
                              type="password"
                              disabled={password}
                              value={password}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div className="form-group required">
                          <div className="row ml-4">
                            <label for="mobileNo" className="control-label">
                              Phone:
                            </label>
                            <div className="col-sm-10 ml-5">
                              <input
                                className="form-control"
                                placeholder="Enter your MobileNo"
                                name="mobile"
                                type="number"
                                value={mobile}
                                onChange={this.handleChange}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="form-group required">
                          <div className="row mb-3 ml-4 mt-3">
                            <label className="control-label">Address:</label>
                            <div className="row ml-4">
                              <div className="col ml-1">
                                <input
                                  className="form-control"
                                  placeholder="Enter Country"
                                  name="country"
                                  type="text"
                                  disabled={country}
                                  value={country}
                                  onChange={this.handleChange}
                                />
                              </div>
                              <div className="col">
                                <select
                                  className="browser-default custom-select"
                                  name="state"
                                  value={state}
                                  onChange={this.handleChange}
                                >
                                  <option value="">Select State</option>
                                  {states.map((obj) => (
                                    <option>{obj}</option>
                                  ))}
                                </select>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div className="row ml-5">
                          <div className="col-1"></div>
                          <div className="col ml-4">
                            <input
                              className="form-control"
                              placeholder="Enter City"
                              name="city"
                              type="text"
                              value={city}
                              onChange={this.handleChange}
                            />
                          </div>
                          <div className="col-5">
                            <input
                              className="form-control"
                              placeholder="Enter Pincode"
                              name="pincode"
                              type="text"
                              value={pincode}
                              onChange={this.handleChange}
                            />
                          </div>
                        </div>
                        <div className="row mt-3">
                          <div className="col-2"></div>
                          <div className="col ml-4">
                            <textarea
                              name="address"
                              type="text"
                              rows="3"
                              cols="70"
                              placeholder="Enter address"
                              value={address}
                              onChange={this.handleChange}
                            ></textarea>
                          </div>
                        </div>
                        <div className="row mt-3 mb-5">
                          <div className="col-3"></div>
                          <div className="col-6">
                            <button
                              type="submit"
                              className="btn btn-outline-danger m-2"
                              onClick={this.handleSubmit}
                            >
                              Submit
                            </button>
                            <button
                              type="button"
                              className="btn btn-outline-danger m-2"
                              onClick={() => this.handleView(0)}
                            >
                              Cancel
                            </button>
                          </div>
                        </div>
                      </form>
                    </div>
                    ;
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-12">
            <div className="row bg-white ml-1 mr-1 mt-5" id="flight">
              <div className="col">
                <div className="row">
                  <div className="col-12 text-center user-logo">
                    <i className="fa fa-user-circle"></i>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 text-center fs-1rem">
                    Mr.{user.fname}&nbsp;{user.lname}
                  </div>
                </div>
                <div className="row">
                  <hr className="col-9 text-center" />
                </div>
                <div className="row">
                  <div className="col-12 text-center fs-1rem">{user.email}</div>
                </div>
                <div className="row">
                  <div className="col-12 text-center fs-1rem">
                    Phone: {user.mobile}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row icons">
          <div className="col-4"></div>
          <div className="col-4">
            <span className="icon-footer">
              <i className="fab fa-facebook-f icon-f"></i>
            </span>
            <span className="icon-footer">
              <i className="fab fa-twitter icon-t"></i>
            </span>
            <span className="icon-footer">
              <i className="fab fa-youtube icon-y"></i>
            </span>
          </div>
          <div className="col-4"></div>
        </div>
        <div className="row">
          <div className="col text-center copyright">
            <p>
              Copyright © 2021 Yatra Online Private Limited, India. All rights
              reserved
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default Profile;
