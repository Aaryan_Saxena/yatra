import React, { Component } from "react";
import { Redirect, Switch, Route } from "react-router-dom";
import ShowNavbar from "./showNavbar";
import "./navbar.css";
import Home from "./home";
import ShowFlights from "./showFlights";
import Login from "./login";
import Booking from "./booking";
import Payment from "./payment";
import authService from "./authService";
import ShowBookings from "./showBookings";
import Logout from "./logout";
import ECash from "./eCash";
import Profile from "./profile";
class YatraComp extends Component {
  render() {
    let user = authService.getUser();
    return (
      <React.Fragment>
        <ShowNavbar user={user} />
        <Switch>
          <Route
            path="/yatra/airSearch"
            render={(props) => <ShowFlights {...props} />}
          />
          <Route
            path="/yatra/allBookings"
            render={(props) => <ShowBookings {...props} />}
          />
          <Route path="/yatra/eCash" render={(props) => <ECash {...props} />} />
          <Route
            path="/yatra/profile"
            render={(props) => <Profile {...props} />}
          />
          <Route
            path="/yatra"
            render={(props) => (
              <Home {...props} handleFlights={this.handleFlights} />
            )}
          />
          <Route path="/booking" render={(props) => <Booking {...props} />} />
          <Route path="/payment" component={Payment} />
          <Route path="/common" component={Login} />
          <Route path="/logout" component={Logout} />
          <Redirect from="/" to="/yatra" />
        </Switch>
      </React.Fragment>
    );
  }
}
export default YatraComp;
