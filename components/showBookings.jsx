import React, { Component } from "react";
import httpService from "./httpService";
import { Link } from "react-router-dom";
import authService from "./authService";
import "./navbar.css";
import "./home.css";
import LeftPanel from "./leftpanel";
class ShowBookings extends Component {
  state = {
    header: [
      "ALL",
      "FLIGHTS",
      "HOTELS",
      "HOMESTAYS",
      "FLIGHTS + HOTELS",
      "BUSES",
      "TRAINS",
      "ACTIVITIES",
      "HOLIDAYS",
    ],
    weekDay: ["Sun", "Mon", "Tues", "Wed", "Thr", "Fri", "Sat"],
    booking: [],
    user: {},
    page: 1,
    fareDetails: {},
    fareDetView: 0,
  };
  async componentDidMount() {
    let user = authService.getUser();
    let userDetails = await httpService.get(`/getUser/${user.id}`);
    let response = await httpService.get("/allBookings");
    console.log(response);
    this.setState({ booking: response.data, user: userDetails.data });
  }
  handlePage = (incr) => {
    let s1 = { ...this.state };
    s1.page = s1.page + incr;
    this.setState(s1);
  };
  handleFareDetails = (details) =>
    this.setState({ fareDetails: details, fareDetView: 1 });
  handleRefresh = () => this.setState({ fareDetView: 0, fareDetails: {} });

  render() {
    let {
      header,
      booking = [],
      page,
      fareDetails = {},
      fareDetView,
      weekDay,
      user = {},
    } = this.state;
    let pageNum = page;
    let pageSize = 1;
    let startIndex = (pageNum - 1) * pageSize;
    let totalNum = 0;
    totalNum = booking.length;
    let pageArr = [...booking];
    let copyArr = pageArr.splice(startIndex, pageSize);
    var totalPage = totalNum % pageSize;
    totalPage =
      totalPage > 0 ? Math.floor(totalNum / 1) + 1 : Math.floor(totalNum / 1);
    if (fareDetails.totalFare) {
      let date = `${fareDetails.currentDate.dd} ${fareDetails.currentDate.mm}, ${fareDetails.currentDate.yy}`;
      date = new Date(date);
      var day = String(date.getDay()).padStart(2, "0");
      day = weekDay.find((obj, index) => (index === +day ? obj : ""));
      console.log(day);
    }
    return (
      <div className="container-fluid" style={{ minWidth: "978px" }}>
        <div className="row">
          <div className="col-9">
            <div className="row mt-3 mb-2">
              <div className="col-9 ml-1">
                Dashboard /{" "}
                <Link to="/yatra/allBookings" className="text-decoration-none">
                  <span onClick={() => this.handleRefresh()}>My Bookings</span>
                </Link>{" "}
                {fareDetails.totalFare && " / Fare Details"}
              </div>
            </div>
            <div className="row ml-1" id="payment">
              <LeftPanel option={"My Bookings"} />
              <div className="col-9">
                {fareDetView === 0 ? (
                  <React.Fragment>
                    <div className="row bg-light fs-14">
                      {header.map((obj) => (
                        <div className="m-2 text-secondary">{obj}</div>
                      ))}
                    </div>
                    <div className="row mt-2 fs-17">
                      <div className="col-3 offset-9">Filter/ Sort By</div>
                    </div>
                    <div className="row">
                      <div className="col-3 offset-9">
                        <div className="form-group">
                          <select className="form-control">
                            <option value="Relevance">Relevance</option>
                            <option value="Upcoming">Upcoming</option>
                            <option value="Completed">Completed</option>
                            <option value="Cancelled">Cancelled</option>
                            <option value="Booking Date">Booking Date</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    {copyArr.map((bk) => (
                      <React.Fragment>
                        <div className="row  pt-1 pb-1 ml-1 mr-1 payment-background">
                          <div className="col-1 text-center mt-1 d-none d-lg-block">
                            <i className="fa fa-fighter-jet fighter-jet-logo"></i>
                          </div>
                          <div className="col-4">
                            <div className="row">
                              <b>{bk.goFlight.name}</b>
                            </div>
                            <div className="row" id="abc10">
                              <div className="h6 text-secondary">
                                {bk.members.adult +
                                  bk.members.child +
                                  bk.members.infant}{" "}
                                Traveller(s)
                              </div>
                            </div>
                          </div>
                          <div className="mt-1 ml-1 text-secondary">
                            Booked On:{bk.currentDate.dd} {bk.currentDate.mm}{" "}
                            {bk.currentDate.yy}| Booking Ref No. {bk.refNo}
                          </div>
                        </div>
                        <div className="row border ml-1 mr-1">
                          <div class="col-4">
                            <b>Departure</b>
                            <div className="text-secondary">
                              {bk.goFlight.desDept}
                            </div>
                          </div>
                          <div className="col-3">
                            <b>Arrival</b>
                            <div className="text-secondary">
                              {bk.goFlight.desArr}
                            </div>
                          </div>
                          <div className="col-1 border-inset"></div>
                          <div className="col-4">
                            <button
                              className="btn btn-danger btn-sm mt-1"
                              onClick={() => this.handleFareDetails(bk)}
                            >
                              Fare Details
                            </button>
                            <button
                              className="btn btn-danger ml-2 btn-sm mt-1"
                              onClick={() => this.handleFareDetails(bk)}
                            >
                              Itinerary
                            </button>
                          </div>
                          <hr class="col-10" />
                          <div className="col-1"></div>
                          <div className="col-3 text-center ml-1 ">
                            <i className="fas fa-file-invoice icon-sm"></i>
                            <div className="fs-12 font-lightestgrey">
                              Print Invoice
                            </div>
                          </div>
                          <div className="border-inset"></div>
                          <div className="col-3 text-center">
                            <i className="fa fa-location-arrow icon-sm"></i>
                            <div className="fs-12 font-lightestgrey">
                              Get Directions
                            </div>
                          </div>
                          <div className="border-inset"></div>
                          <div className="col-2  text-center">
                            <i className="fas fa-sync-alt icon-sm"></i>
                            <div className="fs-12 font-lightestgrey">
                              Re-Book
                            </div>
                          </div>
                        </div>
                      </React.Fragment>
                    ))}
                    <div className="row mt-2">
                      <div className="col-6 text-left">
                        Showing {page} of {totalPage}
                      </div>
                      <div className="col-6 text-right">
                        {page > 1 && (
                          <span
                            className="footer-icon-p"
                            onClick={() => this.handlePage(-1)}
                          >
                            <i className="fas fa-chevron-left"></i>
                          </span>
                        )}
                        <span className="ml-1 mr-1">{page}</span>
                        {totalPage === +page ? (
                          ""
                        ) : (
                          <span
                            className="footer-icon-p"
                            onClick={() => this.handlePage(1)}
                          >
                            <i className="fas fa-chevron-right"></i>
                          </span>
                        )}
                      </div>
                    </div>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <div className="row mt-2 pt-1 pb-1 ml-1 mr-1 payment-background">
                      <div className="col-1 text-center mt-1 d-none d-lg-block">
                        <i className="fa fa-hotel fighter-jet-logo"></i>
                      </div>
                      <div className="col-7 mt-1">
                        <div className="row">
                          <b>{fareDetails.goFlight.name}</b>
                        </div>
                      </div>
                      <div className="mt-1 ml-3 text-secondary">
                        Booked On:&nbsp;{day},&nbsp;{fareDetails.currentDate.dd}{" "}
                        {fareDetails.currentDate.mm}{" "}
                        {fareDetails.currentDate.yy} | Booking Ref No.
                        {fareDetails.refNo}
                      </div>
                    </div>
                    <div className="row border ml-1 mr-1">
                      <div className="col-4 mt-4 mb-3">
                        <b>Check In</b>
                        <div className="text-secondary fs-12">
                          {fareDetails.goFlight.desDept}&nbsp;
                        </div>
                      </div>
                      <div className="col-4 mt-4 mb-3">
                        <b>Check Out</b>
                        <div className="text-secondary fs-12">
                          {fareDetails.goFlight.desArr}&nbsp;
                        </div>
                      </div>
                      <div className="col-3 col-lg-2 mt-4 mb-3">
                        <b>Traveller(s)</b>
                        <div className="text-secondary fs-10">
                          {fareDetails.members.adult +
                            fareDetails.members.child +
                            fareDetails.members.infant}
                        </div>
                      </div>
                      <div className="col-2 mt-4 mb-3">
                        <b>Status</b>
                        <div className="text-secondary fs-12">Booking</div>
                      </div>
                      <hr className="col-11" />
                      <div className="col-2 text-center">
                        <span className="fa-stack invoice-logo">
                          <i className="far fa-circle fa-stack-2x"></i>
                          <i className="fas fa-file-invoice fa-stack-1x"></i>
                        </span>
                        <div>Print Invoice</div>
                      </div>
                      <div className="border-inset"></div>
                      <div className="col-3 text-center">
                        <span className="fa-stack invoice-logo">
                          <i className="far fa-circle fa-stack-2x"></i>
                          <i className="fa fa-location-arrow fa-stack-1x"></i>
                        </span>
                        <div>Get Directions</div>
                      </div>
                      <div className="border-inset"></div>
                      <div className="col-2 text-center">
                        <span className="fa-stack invoice-logo">
                          <i className="far fa-circle fa-stack-2x"></i>
                          <i className="fas fa-sync-alt fa-stack-1x"></i>
                        </span>
                        <div>Re-Book</div>
                      </div>
                      <div className="border-inset"></div>
                      <div className="col-2  text-center">
                        <span className="fa-stack invoice-logo">
                          <i className="far fa-circle fa-stack-2x"></i>
                          <i className="fas fa-file-alt fa-stack-1x"></i>
                        </span>
                        <div>Fare Details</div>
                      </div>
                      <div className="border-inset"></div>
                      <div className="col-2  text-center">
                        <span className="fa-stack invoice-logo">
                          <i className="far fa-circle fa-stack-2x"></i>
                          <i className="fas fa-pen fa-stack-1x"></i>
                        </span>
                        <div>Write To Us</div>
                      </div>
                    </div>
                    <div className="row border ml-1 mr-1 mt-4">
                      <b className="ml-3 mt-2">Your Booking Details</b>
                      <hr className="col-11" />
                      <div className="col-3 col-lg-4 mt-1 mb-3">
                        <i className="fa fa-hotel payment-background"></i>
                        &nbsp;
                        <b className="fs-14">Departure Flight</b>
                        <div className="text-secondary fs-14">
                          {fareDetails.goFlight.name}{" "}
                          {fareDetails.goFlight.code}
                        </div>
                      </div>
                      <div className="col-2 col-lg-2 mt-1 mb-3">
                        <b>Duration</b>
                        <div className="text-secondary fs-12">
                          {fareDetails.goFlight.total}
                        </div>
                      </div>
                      <div className="col-4 col-lg-3 mt-1 mb-3">
                        <b>Departure</b>
                        <div className="text-secondary fs-12">
                          {fareDetails.goFlight.timeDept}{" "}
                          {fareDetails.goFlight.T1}
                        </div>
                      </div>
                      <div className="col-4 mt-1 col-lg-3 mb-3">
                        <b>Arrival</b>
                        <div className="text-secondary fs-12">
                          {fareDetails.goFlight.timeArr}{" "}
                          {fareDetails.goFlight.T2}
                        </div>
                      </div>
                    </div>
                    {fareDetails.rtnFlight.id && (
                      <div className="row border ml-1 mr-1 mt-4">
                        <div className="col-3 col-lg-4 mt-1 mb-3">
                          <i className="fa fa-hotel payment-background"></i>
                          &nbsp;
                          <b className="fs-14">Return Flight</b>
                          <div className="text-secondary fs-14">
                            {fareDetails.rtnFlight.name}{" "}
                            {fareDetails.rtnFlight.code}
                          </div>
                        </div>
                        <div className="col-2 col-lg-2 mt-1 mb-3">
                          <b>Duration</b>
                          <div className="text-secondary fs-12">
                            {fareDetails.rtnFlight.total}
                          </div>
                        </div>
                        <div className="col-4 col-lg-3 mt-1 mb-3">
                          <b>Departure</b>
                          <div className="text-secondary fs-12">
                            {fareDetails.rtnFlight.timeDept}{" "}
                            {fareDetails.rtnFlight.T1}
                          </div>
                        </div>
                        <div className="col-4 mt-1 col-lg-3 mb-3">
                          <b>Arrival</b>
                          <div className="text-secondary fs-12">
                            {fareDetails.rtnFlight.timeArr}{" "}
                            {fareDetails.rtnFlight.T2}
                          </div>
                        </div>
                      </div>
                    )}
                    <div className="row ml-4 col-lg-11 border col-12 mr-2 mt-3 mb-3">
                      <div className="col-3 mt-3 mb-3 ml-2">
                        <b>Inclusions</b>
                      </div>
                      <div className="col-6"></div>
                      <div className="col-9 mb-4 text-secondary">
                        <i
                          className="fa fa-check"
                          style={{ color: "grey" }}
                        ></i>
                        &nbsp; Breakfast, Complimentary WiFi Internet
                      </div>
                    </div>
                    <div className="row ml-1 mt-3  mr-2 col-lg-11 col-12">
                      <div className="col-3 border">
                        <b>Traveller</b>
                      </div>
                      <div className="col-4 border">
                        <b>Type</b>
                      </div>
                      <div className="col-2 border">
                        <b>Baggage</b>
                      </div>
                    </div>
                    {fareDetails.travellerInfo.adult.map((obj) => (
                      <div className="row ml-1  mr-2  col-lg-11 col-12">
                        <div className="col-3 border">
                          {obj.firstName} {obj.lastName}
                        </div>
                        <div className="col-4 border">
                          {fareDetails.members.class}
                        </div>
                        <div className="col-2 border">25 kgs</div>
                      </div>
                    ))}
                    {fareDetails.travellerInfo.child.map((obj) => (
                      <div className="row ml-1  mr-2  col-lg-11 col-12">
                        <div className="col-3 border">
                          {obj.firstName} {obj.lastName}
                        </div>
                        <div className="col-4 border">
                          {fareDetails.members.class}
                        </div>
                        <div class="col-2 border">25 kgs</div>
                      </div>
                    ))}
                    {fareDetails.travellerInfo.infant.map((obj) => (
                      <div className="row ml-1  mr-2  col-lg-11 col-12">
                        <div className="col-3 border">
                          {obj.firstName} {obj.lastName}
                        </div>
                        <div className="col-4 border">
                          {fareDetails.members.class}
                        </div>
                        <div className="col-2 border">25 kgs</div>
                      </div>
                    ))}
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-12">
            <div className="row bg-white ml-1 mr-1 mt-5" id="flight">
              <div className="col">
                <div className="row">
                  <div className="col-12 text-center user-logo">
                    <i className="fa fa-user-circle"></i>
                  </div>
                </div>
                <div className="row">
                  <div className="col-12 text-center fs-1rem">
                    Mr.{user.fname}&nbsp;{user.lname}
                  </div>
                </div>
                <div className="row">
                  <hr className="col-9 text-center" />
                </div>
                <div className="row">
                  <div className="col-12 text-center fs-1rem">{user.email}</div>
                </div>
                <div className="row">
                  <div className="col-12 text-center fs-1rem">
                    Phone: {user.mobile}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row icons">
          <div className="col-4"></div>
          <div className="col-4">
            <span className="icon-footer">
              <i className="fab fa-facebook-f icon-f"></i>
            </span>
            <span className="icon-footer">
              <i className="fab fa-twitter icon-t"></i>
            </span>
            <span className="icon-footer">
              <i className="fab fa-youtube icon-y"></i>
            </span>
          </div>
          <div className="col-4"></div>
        </div>
        <div className="row">
          <div className="col text-center copyright">
            <p>
              Copyright © 2021 Yatra Online Private Limited, India. All rights
              reserved
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default ShowBookings;
