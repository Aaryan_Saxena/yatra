import React, { Component } from "react";
import "./navbar.css";
import "./home.css";
import $ from "jquery";
import Calendar from "react-calendar";
import "react-calendar/dist/Calendar.css";
import httpService from "./httpService";
class Home extends Component {
  state = {
    domFlights: [
      {
        origin: "New Delhi",
        dest: "Bengaluru",
        date: "Wed, 3 Oct",
        amount: 3590,
      },
      {
        origin: "New Delhi",
        dest: "Mumbai",
        date: "Sun, 13 Oct",
        amount: 2890,
      },
      {
        origin: "Hyderabad",
        dest: "Bengaluru",
        date: "Mon,30 Sep",
        amount: 2150,
      },
      { origin: "Mumbai", dest: "Pune", date: "Sun,6 Oct", amount: 1850 },
    ],
    holidayDestn: [
      {
        img: "https://i.ibb.co/SQ7NSZT/hol1.png",
        place: "Australia",
        price: "177,990",
        days: "9 Nights / 10 Days",
      },
      {
        img: "https://i.ibb.co/Wxj50q1/hol2.png",
        place: "Europe",
        price: "119,990",
        days: "6 Nights / 7 Days",
      },
      {
        img: "https://i.ibb.co/VY3XNZr/hol3.png",
        place: "New Zealand",
        price: "199,990",
        days: "6 Nights / 7 Days",
      },
      {
        img: "https://i.ibb.co/j4NNc35/hol4.jpg",
        place: "Sri Lanka",
        price: "18,999",
        days: "4 Nights / 5 Days",
      },
      {
        img: "https://i.ibb.co/ct6076f/hol5.jpg",
        place: "Kerala",
        price: "12,999",
        days: "4 Nights / 5 Days",
      },
      {
        img: "https://i.ibb.co/vB0CpYK/hol6.jpg",
        place: "Char Dham",
        price: "22,999",
        days: "4 Nights / 5 Days",
      },
    ],
    month: [
      "January",
      "Feberuary",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    weekDay: [
      "Sunday",
      "Monday",
      "Tuesday",
      "Wednesday",
      "Thrusday",
      "Friday",
      "Saturday",
    ],
    selectedFrom: "",
    selectedTo: "",
    tickets: { class: "Economy", adult: 1, child: 0, infant: 0 },
    date: new Date(),
    returnDate: "",
    ticket: "One Way",
    ticketType: "Flights",
  };
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps, prevState) {
    if (prevState.ticketType !== this.state.ticketType) this.fetchData();
  }
  fetchData() {
    $(document).ready(function (e) {
      $("#tickets").hide();
      $("#calendar").hide();
      $("#showMem").click(function (e) {
        if (e.target.className === "fa fa-chevron-down") {
          $("#showMem").removeClass("fa-chevron-down");
          $("#showMem").addClass("fa-chevron-up");
        } else {
          $("#showMem").removeClass("fa-chevron-up");
          $("#showMem").addClass("fa-chevron-down");
        }
        $("#tickets").toggle();
      });
    });
    let today = new Date();
    var nextDay = new Date(today);
    nextDay.setDate(today.getDate() + 1);
    this.setState({ returnDate: nextDay });
    $("#show-calendar").click(function (e) {
      $("#calendar").toggle();
    });
  }
  handleReturnBlock = () => {
    $("#return-calendar").toggle();
  };
  handleSwapping = () => {
    let s1 = { ...this.state };
    let x = s1.selectedFrom;
    s1.selectedFrom = s1.selectedTo;
    s1.selectedTo = x;
    this.setState(s1);
  };
  handleChange = (e) => {
    let { currentTarget: input } = e;
    let s1 = { ...this.state };
    input.name === "class"
      ? (s1.tickets[input.name] = input.value)
      : (s1[input.name] = input.value);
    if (input.name === "class") {
      s1.tickets.adult = 1;
      s1.tickets.child = 0;
      s1.tickets.infant = 0;
    }
    this.setState(s1);

    if (s1.selectedFrom && s1.selectedTo && s1.selectedFrom === s1.selectedTo) {
      alert("Destination and Arrival have to be different");
      s1.selectedTo = "";
      s1.selectedFrom = "";
      this.setState(s1);
    }
  };
  handleDate = (date) => this.setState({ date: date });
  handleReturnDate = (date) => this.setState({ returnDate: date });
  handleTickets = (name, incr) => {
    let s1 = { ...this.state };
    s1.tickets[name] = s1.tickets[name] + incr;
    this.setState(s1);
  };
  handleTicketClass = (cls) =>
    this.state.ticket === cls
      ? "be-container-snipe-active"
      : "be-container-snipe";
  handleTicket = (cls) => {
    this.setState({ ticket: cls });
  };
  handleTicketTypeCls = (cls) =>
    this.state.ticketType === cls ? "ticketType-active" : "ticketType";
  handleTicketType = (type) => {
    this.setState({ ticketType: type });
  };
  showFlights = () => {
    let s1 = { ...this.state };
    let today = s1.date;
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth()).padStart(2, "0");
    var yy = String(today.getFullYear()).padStart(2, "0");
    mm = s1.month.find((obj, index) => (index === +mm ? obj : ""));
    let tommorow = s1.returnDate;
    var rdd = String(tommorow.getDate()).padStart(2, "0");
    var rmm = String(tommorow.getMonth()).padStart(2, "0");
    var ryy = String(tommorow.getFullYear()).padStart(2, "0");
    rmm = s1.month.find((obj, index) => (index === +rmm ? obj : ""));
    let json = {
      from: s1.selectedFrom,
      to: s1.selectedTo,
      currentDate: { dd: dd, mm: mm, yy: yy },
      returnDate: s1.ticket === "Return" ? { dd: rdd, mm: rmm, yy: ryy } : {},
      tickets: s1.tickets,
    };
    this.handleFlights("/srchFlight", json);
  };
  async handleFlights(url, obj) {
    let response = await httpService.post(url, obj);
    this.props.history.push("/yatra/airSearch");
  }
  render() {
    let {
      domFlights,
      holidayDestn,
      month,
      weekDay,
      selectedTo = "",
      selectedFrom = "",
      tickets,
      date,
      returnDate = "",
      ticketType,
      ticket,
    } = this.state;
    var today = date;
    var dd = String(today.getDate()).padStart(2, "0");
    var mm = String(today.getMonth()).padStart(2, "0");
    var yy = String(today.getFullYear()).padStart(2, "0");
    var day = String(today.getDay()).padStart(2, "0");
    mm = month.find((obj, index) => (index === +mm ? obj : ""));
    day = weekDay.find((obj, index) => (index === +day ? obj : ""));
    let totTickets = tickets.adult + tickets.child + tickets.infant;
    let nextDay = returnDate;
    if (nextDay !== "") {
      var rdd = String(nextDay.getDate()).padStart(2, "0");
      var rmm = String(nextDay.getMonth()).padStart(2, "0");
      var ryy = String(nextDay.getFullYear()).padStart(2, "0");
      var rday = String(nextDay.getDay()).padStart(2, "0");
      rmm = month.find((obj, index) => (index === +rmm ? obj : ""));
      rday = weekDay.find((obj, index) => (index === +rday ? obj : ""));
    }
    return (
      <div className="container-fluid" style={{ minWidth: "978px" }}>
        <div className="row mt-4">
          <div className="col-lg-5 col-sm-12" align="center">
            <div className="container-fluid smooth-banner-transition">
              <div className="row mt-3 mb-3">
                <div className="col-3">
                  <span
                    className={
                      "fa-stack fa-2x cursor-pointer " +
                      this.handleTicketTypeCls("Flights")
                    }
                    onClick={() => this.handleTicketType("Flights")}
                  >
                    <i className="fa fa-circle fa-stack-2x"></i>
                    <i className="fas fa-plane fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
                <div className="col-3">
                  <span
                    className={
                      "fa-stack fa-2x cursor-pointer " +
                      this.handleTicketTypeCls("Hotels")
                    }
                    onClick={() => this.handleTicketType("Hotels")}
                  >
                    <i className="fa fa-circle  fa-stack-2x"></i>
                    <i className="fa fa-bed fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
                <div className="col-3">
                  <span
                    className={
                      "fa-stack fa-2x cursor-pointer " +
                      this.handleTicketTypeCls("Buses")
                    }
                    onClick={() => this.handleTicketType("Buses")}
                  >
                    <i className="fa fa-circle  fa-stack-2x"></i>
                    <i className="fa fa-bus fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
                <div className="col-3">
                  <span
                    className={
                      "fa-stack fa-2x cursor-pointer " +
                      this.handleTicketTypeCls("Taxis")
                    }
                    onClick={() => this.handleTicketType("Taxis")}
                  >
                    <i className="fa fa-circle fa-stack-2x"></i>
                    <i className="fa fa-taxi fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              </div>
              <div className="row">
                <div className="col text-center text-dark">
                  <h4>{ticketType}</h4>
                </div>
              </div>
              {ticketType === "Flights" && (
                <div className="row">
                  <div className="container">
                    <div className="row mb-3 mt-2">
                      <div className="col-12 text-center">
                        <button
                          type="button"
                          className={"" + this.handleTicketClass("One Way")}
                          onClick={() => this.handleTicket("One Way")}
                        >
                          One Way
                        </button>
                        &nbsp;
                        <button
                          type="button"
                          className={"" + this.handleTicketClass("Return")}
                          onClick={() => this.handleTicket("Return")}
                        >
                          Return
                        </button>
                      </div>
                    </div>
                    <form>
                      <div className="row mt-2 mb-3">
                        <div className="col-5">
                          <span className="gtyupi">Depart From</span>
                          <div>
                            <select
                              className="yt-input-text"
                              id="selectedFrom"
                              name="selectedFrom"
                              value={selectedFrom}
                              onChange={this.handleChange}
                            >
                              <option>Select city</option>
                              <option>New Delhi (DEL)</option>
                              <option>Mumbai (BOM)</option>
                              <option>Banglore (BLR)</option>
                              <option>Kolkata (CCU)</option>
                            </select>
                          </div>
                        </div>
                        <div className="col-2">
                          <span
                            data-swap="true"
                            tabindex="-1"
                            className="yt-booking-engine-snipe cursor-pointer"
                            onClick={() => this.handleSwapping()}
                          >
                            <img
                              title="Swap Origin City and Destination City"
                              className="beSwapCity"
                              src="https://www.yatra.com/fresco/resources/toucan/dist/images/swipe.svg?17fd684eff42c5149d5fd6cfe4b0b38b"
                            />
                          </span>
                        </div>
                        <div className="col-5">
                          <span className="gtyupi">Going To</span>
                          <div>
                            <select
                              className="yt-input-text"
                              id="selectedTo"
                              name="selectedTo"
                              value={selectedTo}
                              onChange={this.handleChange}
                            >
                              <option>Select city</option>
                              <option>New Delhi (DEL)</option>
                              <option>Mumbai (BOM)</option>
                              <option>Banglore (BLR)</option>
                              <option>Kolkata (CCU)</option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="row mt-2 mb-3">
                        <div className="text-danger col-12"></div>
                      </div>
                      <div className="row mt-2 mb-3 text-left">
                        <div className="col-6 cursor-pointer">
                          <div>
                            <div id="show-calendar">
                              <span className="gtyupi">Departure Date</span>
                              <div className="date-text">
                                {dd} {mm}, {yy}
                              </div>
                              <div>{day}</div>
                            </div>
                            <div id="calendar">
                              <Calendar
                                value={date}
                                onChange={this.handleDate}
                                minDate={new Date()}
                                showNeighboringMonth={false}
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-6">
                          <span className="gtyupi">Return Date</span>
                          {ticket === "One Way" ? (
                            <p>
                              <a href="#" className="text-decoration-none">
                                <small>Book round trip to save extra</small>
                              </a>
                            </p>
                          ) : (
                            <div>
                              <div
                                id="show-return-calendar cursor-pointer"
                                onClick={() => this.handleReturnBlock()}
                              >
                                <div className="date-text">
                                  {rdd} {rmm}, {ryy}
                                </div>
                                <div>{rday}</div>
                              </div>
                              <div
                                id="return-calendar"
                                className="return-calendar-none"
                              >
                                <Calendar
                                  value={returnDate}
                                  onChange={this.handleReturnDate}
                                  minDate={new Date()}
                                  showNeighboringMonth={false}
                                />
                              </div>
                            </div>
                          )}
                        </div>
                      </div>
                      <hr />
                      <div className="row mt-2 text-left">
                        <div className="col-8">
                          <span className="gtyupi">Travellers, class</span>
                          <div className="traveller-text">
                            {totTickets} Traveller,{tickets.class}
                          </div>
                        </div>
                        <div className="col-1 offset-3">
                          <i
                            className="fa fa-chevron-down cursor-pointer"
                            id="showMem"
                          ></i>
                        </div>
                      </div>
                      <div id="tickets">
                        <div className="row mt-2">
                          <div className="col-4">
                            <h6>Adult</h6>
                            <div className="row ml-1">
                              <h6 className="col-1 border">
                                <small
                                  className="text-muted cursor-pointer"
                                  onClick={() =>
                                    this.handleTickets("adult", -1)
                                  }
                                >
                                  -
                                </small>
                              </h6>
                              <h6 className="col-1 border bg-light">
                                <small className="text-muted">
                                  {tickets.adult}
                                </small>
                              </h6>
                              <h6
                                className="col-1 border"
                                onClick={() => this.handleTickets("adult", 1)}
                              >
                                <small className="text-muted cursor-pointer">
                                  +
                                </small>
                              </h6>
                            </div>
                          </div>
                          <div class="col-4">
                            <h6>Child(2-12 yrs)</h6>
                            <div class="row ml-1">
                              <h6 class="col-1 border">
                                <small
                                  className="text-muted cursor-pointer"
                                  onClick={() =>
                                    this.handleTickets("child", -1)
                                  }
                                >
                                  -
                                </small>
                              </h6>
                              <h6 className="col-1 border bg-light">
                                <small className="text-muted">
                                  {tickets.child}
                                </small>
                              </h6>
                              <h6 className="col-1 border">
                                <small
                                  className="text-muted cursor-pointer"
                                  onClick={() => this.handleTickets("child", 1)}
                                >
                                  +
                                </small>
                              </h6>
                            </div>
                          </div>
                          <div class="col-4">
                            <h6>Infant(Below 2 yrs)</h6>
                            <div className="row ml-1">
                              <h6 className="col-1 border">
                                <small
                                  className="text-muted cursor-pointer"
                                  onClick={() =>
                                    this.handleTickets("infant", -1)
                                  }
                                >
                                  -
                                </small>
                              </h6>
                              <h6 className="col-1 border bg-light">
                                <small className="text-muted">
                                  {tickets.infant}
                                </small>
                              </h6>
                              <h6 className="col-1 border">
                                <small
                                  className="text-muted cursor-pointer"
                                  onClick={() =>
                                    this.handleTickets("infant", 1)
                                  }
                                >
                                  +
                                </small>
                              </h6>
                            </div>
                          </div>
                        </div>
                        <div className="row mt-2">
                          <div className="ml-2 text-left">
                            <div className="form-check mt-1">
                              <input
                                name="class"
                                type="radio"
                                className="form-check-input"
                                value="Economy"
                                checked={tickets.class === "Economy"}
                                onChange={this.handleChange}
                              />
                              <label
                                className="form-check-label mt-1"
                                for="Economy"
                              >
                                Economy
                              </label>
                            </div>
                            <div className="form-check mt-1">
                              <input
                                name="class"
                                type="radio"
                                className="form-check-input"
                                value="Premium Economy"
                                checked={tickets.class === "Premium Economy"}
                                onChange={this.handleChange}
                              />
                              <label
                                className="form-check-label mt-1"
                                for="Premium Economy"
                              >
                                Premium Economy
                              </label>
                            </div>
                            <div className="form-check mt-1">
                              <input
                                name="class"
                                type="radio"
                                className="form-check-input"
                                value="Business"
                                checked={tickets.class === "Business"}
                                onChange={this.handleChange}
                              />
                              <label
                                className="form-check-label mt-1"
                                for="Business"
                              >
                                Business
                              </label>
                            </div>
                          </div>
                        </div>
                      </div>
                      <hr />
                      <div className="row mt82">
                        <div className="col-6"></div>
                        <div className="col-6">
                          <button
                            className="yt-booking-engine-snipe-button"
                            type="button"
                            disabled={!selectedFrom && !selectedTo}
                            onClick={() => this.showFlights()}
                          >
                            <span className="ml-1">
                              Search Flights
                              <i
                                className="fa fa-arrow-right"
                                aria-hidden="true"
                              ></i>
                            </span>
                          </button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              )}
            </div>
          </div>
          <div className="col-lg-7 col-sm-12 bg-light">
            <h6>Flight Discounts for you</h6>
            <div className="row bg-light">
              <div className="col-4 pr-4">
                <img
                  src="https://i.ibb.co/qdc2z7Z/ad01.png"
                  alt=""
                  className="promo-image"
                />
              </div>
              <div className="col-4 pr-4">
                <img
                  src="https://i.ibb.co/yp0bbgz/ad02.png"
                  alt=""
                  className="promo-image"
                />
              </div>
              <div className="col-4 pr-4">
                <img
                  src="https://i.ibb.co/DkrVrkY/ad03.png"
                  alt=""
                  className="promo-image"
                />
              </div>
              <div className="col-12 mt-2">
                <img
                  src="https://i.ibb.co/Rc9qLyT/banner1.jpg"
                  alt=""
                  className="promo-image"
                />
              </div>
              <div class="col-12 text-muted">
                <h5>Popular Domestic Flight Routes</h5>
              </div>
              <div className="col-12">
                <div className="row">
                  {domFlights.map((fl, index) => (
                    <div
                      className="col-2 bg-white m-1 p-0"
                      style={{ minWidth: "120px" }}
                      key={index}
                    >
                      <div className="row">
                        <div className="col-12 text-dark text-center">
                          <strong>{fl.dest}</strong>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 text-muted text-center">
                          {" "}
                          {fl.date}
                        </div>
                      </div>
                      <br />
                      <div className="row">
                        <div className="col-12 text-dark text-center">
                          <strong> {fl.origin} </strong>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 text-muted text-center">
                          {" "}
                          Starting From{" "}
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-12 text-center">
                          <button className="btn btn-warning">{fl.dest}</button>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
              <div class="col-12 text-muted">
                <h4>Popular Holiday Destinations</h4>
              </div>
              <div className="col-12">
                <div className="row">
                  {holidayDestn.map((hl, index) => (
                    <div
                      className="col-5 bg-white m-1"
                      style={{ minWidth: "250px" }}
                      key={index}
                    >
                      <div className="row">
                        <div className="col-2 mt-3">
                          <img className="img-fluid" src={hl.img} />
                        </div>
                        <div className="col-8">
                          <div className="row">
                            <div class="col-12 text-muted text-center">
                              {" "}
                              {hl.place}
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-12 text-center">
                              <span className="text-danger">
                                <strong> Rs.{hl.price}</strong>
                              </span>
                              <span className="text-dark"> per person</span>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-12 text-center text-muted">
                              {" "}
                              {hl.days}
                            </div>
                          </div>
                        </div>
                        <div className="col-1 mt-3">
                          <i className="fa fa-arrow-right"></i>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Home;
