import React, { Component } from "react";
import httpService from "./httpService";
import { Link } from "react-router-dom";
import authService from "./authService";
import "./navbar.css";
import LeftPanel from "./leftpanel";
class ECash extends Component {
  state = {
    booking: [],
    user: {},
  };
  async componentDidMount() {
    let user = authService.getUser();
    let userDetails = await httpService.get(`/getUser/${user.id}`);
    let response = await httpService.get("/allBookings");
    console.log(response);
    this.setState({ booking: response.data, user: userDetails.data });
  }

  render() {
    let { booking = [], user = {} } = this.state;
    return (
      <div className="container-fluid" style={{ minWidth: "978px" }}>
        <div className="row">
          <div className="col-9">
            <div className="row mt-3 mb-2">
              <div className="col-9 ml-1">
                Dashboard /{" "}
                <Link to="/yatra/eCash" className="text-decoration-none">
                  <span>Your eCash</span>
                </Link>{" "}
              </div>
            </div>
            <div className="row ml-1" id="payment">
              <LeftPanel option={"Your eCash"} />
              <div className="col-9">
                <div className="row bg-light">
                  <div className="col-4 mt-2 mb-2">VIEW ECASH SUMMARY</div>
                  <div className="col-4"></div>
                  <div className="mt-2 mb-2 col-4">CURRENT BALANCE ₹.0</div>
                </div>
                <div className="row mt-3 mb-3 border ml-1 mr-1">
                  <div className="col-3 bg-light text-center">
                    <div className="row mt-4 mb-4 ml-1 mr-1 border bg-white">
                      <div className="text-danger mt-2 ml-4">REDEEMABLE</div>
                      <br />
                      <div className="ml-5">
                        <b>₹0</b>
                      </div>
                      <hr className="col-7" />
                    </div>
                    <div className="row mt-4 mb-4 ml-1 mr-1 border bg-white">
                      <div className="text-danger mt-2 ml-4">PENDING</div>
                      <br />
                      <div className="ml-5">
                        <b>₹0</b>
                      </div>
                      <hr className="col-7" />
                    </div>
                  </div>
                  <div className="col-9">
                    <div className="row mt-3">
                      <div className="col-3">
                        Transferable&nbsp;<i className="fa fa-info-circle"></i>
                      </div>
                      <div className="col-3">
                        <button className="btn btn-danger">Transfer</button>
                      </div>
                      <div className="col-3">
                        Convertible&nbsp;<i className="fa fa-info-circle"></i>
                      </div>
                      <div className="col-3">
                        <button className="btn mr-1">Convert</button>
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-3">₹0</div>
                      <div className="col-3 offset-3">₹0</div>
                    </div>
                    <hr />
                    <div className="row mt-2 mb-2 mr-1">
                      <div className="col-2">Date</div>
                      <div className="col-3">Description</div>
                      <div className="col-4">Reference Number</div>
                      <div className="col-2">Credit</div>
                      <div className="col-1">Debit</div>
                    </div>
                    {booking.map((obj, index) => (
                      <React.Fragment>
                        <hr />
                        <div className="row mt-2 mb-2 mr-1 text-secondary">
                          <div className="col-2">
                            {obj.currentDate.dd} {obj.currentDate.mm}{" "}
                            {obj.currentDate.yy}
                          </div>
                          <div className="col-3">
                            Use Ecash using promocode (Before Jun{" "}
                            {Math.floor(Math.random() * (30 - 1)) + 1},2021 )
                          </div>
                          <div className="col-4 text-truncate">{obj.refNo}</div>
                          <div className="col-2">
                            ₹{index % 2 === 0 ? "250" : "500"}
                          </div>
                          <div className="col-1">₹0</div>
                        </div>
                        <hr />
                      </React.Fragment>
                    ))}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-2 col-12">
            <div className="row bg-white ml-1 mr-1 mt-5" id="flight">
              <div className="col">
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "35px", color: "rgb(243, 79, 79)" }}
                  >
                    <i className="fa fa-user-circle"></i>
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "1rem" }}
                  >
                    Mr.{user.fname}&nbsp;{user.lname}
                  </div>
                </div>
                <div className="row">
                  <hr className="col-9 text-center" />
                </div>
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "1rem" }}
                  >
                    {user.email}
                  </div>
                </div>
                <div className="row">
                  <div
                    className="col-12 text-center"
                    style={{ fontSize: "1rem" }}
                  >
                    Phone: {user.mobile}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="row icons">
          <div className="col-4"></div>
          <div className="col-4">
            <span className="icon-footer">
              <i className="fab fa-facebook-f icon-f"></i>
            </span>
            <span className="icon-footer">
              <i className="fab fa-twitter icon-t"></i>
            </span>
            <span className="icon-footer">
              <i className="fab fa-youtube icon-y"></i>
            </span>
          </div>
          <div className="col-4"></div>
        </div>
        <div className="row">
          <div className="col text-center copyright">
            <p>
              Copyright © 2021 Yatra Online Private Limited, India. All rights
              reserved
            </p>
          </div>
        </div>
      </div>
    );
  }
}
export default ECash;
