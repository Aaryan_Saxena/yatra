import axios from "axios";
/* const baseURL = "http://localhost:2450"; */
const baseURL = "https://ytr-28-server.herokuapp.com";
function get(url) {
  let str = localStorage.getItem("user");
  let user = str ? JSON.parse(str) : null;
  if (user) {
    let token = user.token;
    return axios.get(baseURL + url, {
      headers: { Authorization: `bearer ${token}` },
    });
  } else return axios.get(baseURL + url);
}
function post(url, obj) {
  return axios.post(baseURL + url, obj);
}
function put(url, obj) {
  return axios.put(baseURL + url, obj);
}
function deleteReq(url) {
  return axios.delete(baseURL + url);
}

export default { get, post, put, deleteReq };
