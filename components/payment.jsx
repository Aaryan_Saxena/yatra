import React, { Component } from "react";
import httpService from "./httpService";
import "./navbar.css";
import "./home.css";
import authService from "./authService";
class Payment extends Component {
  state = {
    ticket: {},
    method: "UPI",
  };
  async componentDidMount() {
    let response = await httpService.get("/getBookingSummary");
    console.log(response);
    this.setState({ ticket: response.data });
  }
  handlePaymentMethodCls = (method) =>
    this.state.method === method ? "bg-white" : "bg-light";
  handlePaymentMethod = (method) => this.setState({ method: method });
  handleTicketBooked = () => {
    let user = authService.getUser();
    if (user && user.id) {
      this.postTicket();
    } else {
      alert("Login to Proceed..!");
      window.location = "/common";
    }
  };
  async postTicket() {
    let response = await httpService.post("/ticketBooked", this.state.ticket);
    alert("Ticket Booked Succesfully");
    window.location = "/yatra";
  }
  render() {
    let {
      goFlight = {},
      rtnFlight = {},
      totalFare = 0,
      cntctDet = {},
      members = {},
      travellerInfo = {},
    } = this.state.ticket;
    let { method } = this.state;
    return (
      <div className="container-fluid payment-background">
        <div className="row">
          <div className="col-lg-9 col-12">
            <div className="row mt-2 mb-2">
              <div className="col-9 ml-1 fs-20">
                <strong>
                  <i className="fa fa-credit-card"></i> Payment Method
                </strong>
              </div>
            </div>
            <div className="row bg-white ml-1" id="payment">
              <div className="col-lg-2 col-4 text-center" id="fs20">
                <div
                  className={
                    "border cursor-pointer " +
                    this.handlePaymentMethodCls("UPI")
                  }
                  onClick={() => this.handlePaymentMethod("UPI")}
                >
                  <div className="mt-2 mb-2">UPI</div>
                </div>
                <div
                  className={
                    "border cursor-pointer " +
                    this.handlePaymentMethodCls("Credit Card")
                  }
                  onClick={() => this.handlePaymentMethod("Credit Card")}
                >
                  <div className="mt-2 mb-2">Credit Card</div>
                </div>
                <div
                  className={
                    "border cursor-pointer " +
                    this.handlePaymentMethodCls("Debit Card")
                  }
                  onClick={() => this.handlePaymentMethod("Debit Card")}
                >
                  <div className="mt-2 mb-2">Debit Card</div>
                </div>
                <div
                  className={
                    "border cursor-pointer " +
                    this.handlePaymentMethodCls("Net Banking")
                  }
                  onClick={() => this.handlePaymentMethod("Net Banking")}
                >
                  <div className="mt-2 mb-2">Net Banking</div>
                </div>
                <div
                  className={
                    "border cursor-pointer " +
                    this.handlePaymentMethodCls("PayPal")
                  }
                  onClick={() => this.handlePaymentMethod("PayPal")}
                >
                  <div className="mt-2 mb-2">PayPal</div>
                </div>
              </div>
              <div className="col-8" id="fs20">
                <br />
                <br />
                <div className="row">
                  <div className="col ml-4">
                    <strong>Pay with {method}</strong>
                  </div>
                </div>
                <br />
                <br />
                <div className="row">
                  <div className="col-lg-4 col-5 ml-4 fs-25">₹ {totalFare}</div>
                  <div className="col-6">
                    <button
                      className="btn btn-danger text-white btn-md"
                      onClick={() => this.handleTicketBooked()}
                    >
                      Pay Now
                    </button>
                  </div>
                </div>
                <div className="row">
                  <div className="col text-secondary text-center fs-12">
                    {" "}
                    By clicking Pay Now, you are agreeing to terms and
                    Conditions and Privacy Policy{" "}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-12">
            <div className="row mt-2 mb-2">
              <div className="col-11 ml-1 text-left fs-16">Payment Details</div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="flight">
              <div className="col">
                <div className="row">
                  <div className="col-8 text-left fs-1rem">
                    Total Flight Price
                  </div>
                  <div className="col-4 text-right fs-1rem">
                    {" "}
                    ₹ {totalFare}{" "}
                  </div>
                </div>
                <div className="row">
                  <div className="col-8 text-left fs-1rem">
                    Convenience Fees
                  </div>
                  <div className="col-4 text-right fs-1rem"> ₹ 350</div>
                </div>
                <hr />
                <div className="row">
                  <div
                    className="col-6 text-left"
                    style={{ fontSize: "1.7rem" }}
                  >
                    You Pay
                  </div>
                  <div
                    className="col-6 text-right"
                    style={{ fontSize: "1.7rem" }}
                  >
                    ₹ {totalFare + 350}
                  </div>
                </div>
                <hr />
                <div className="row">
                  <div className="col-6 text-left">Earn eCash</div>
                  <div className="col-6 text-right fs-1rem">₹ 250</div>
                </div>
              </div>
            </div>
            <div className="row mt-1 mb-1">
              <div className="col ml-1"> Booking summary </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="flight">
              <div className="col-12">
                <div className="row">
                  <div className="col-6">
                    <img src={goFlight.logo} className="payment-logo" />
                  </div>
                  <div className="col-5 mr-1 text-right" id="fs20">
                    {goFlight.code}
                  </div>
                </div>
                <div className="row">
                  <div className="col-5 pl-1" id="fs18">
                    {goFlight.desDept}
                  </div>
                  <div className="col-2 text-left">
                    <i className="fa fa-fighter-jet text-grey"></i>
                  </div>
                  <div className="col-5" id="fs18">
                    {goFlight.desArr}
                  </div>
                </div>
              </div>
              {rtnFlight.id && (
                <div className="col-12 mt-3">
                  <div className="row">
                    <div className="col-6">
                      <img src={rtnFlight.logo} className="payment-logo" />
                    </div>
                    <div className="col-5 mr-1 text-right" id="fs20">
                      {rtnFlight.code}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-5 pl-1" id="fs18">
                      {rtnFlight.desDept}
                    </div>
                    <div className="col-2 text-left">
                      <i className="fa fa-fighter-jet text-grey"></i>
                    </div>
                    <div className="col-5" id="fs18">
                      {rtnFlight.desArr}
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="row ml-1 mr-1 mt-1 mb-1">
              <div className="col"> Contact Details </div>
            </div>
            <div className="row bg-white ml-1 mr-1" id="detail">
              <div className="col">
                <div className="row">
                  <div className="col" id="fs16">
                    {travellerInfo.adult &&
                      travellerInfo.adult.length !== 0 && <h7>Adults :</h7>}
                    {travellerInfo.adult &&
                      travellerInfo.adult.length !== 0 &&
                      travellerInfo.adult.map((pr, index) => (
                        <div key={index}>
                          {index + 1}. {pr.firstName} {pr.lastName}
                        </div>
                      ))}
                    {travellerInfo.child &&
                      travellerInfo.child.length !== 0 && <h7>Childs :</h7>}
                    {travellerInfo.child &&
                      travellerInfo.child.length !== 0 &&
                      travellerInfo.child.map((pr, index) => (
                        <div key={index}>
                          {index + 1}. {pr.firstName} {pr.lastName}
                        </div>
                      ))}
                    {travellerInfo.infant &&
                      travellerInfo.infant.length !== 0 && <h7>Infant :</h7>}
                    {travellerInfo.infant &&
                      travellerInfo.infant.length !== 0 &&
                      travellerInfo.infant.map((pr, index) => (
                        <div key={index}>
                          {index + 1}. {pr.firstName} {pr.lastName}
                        </div>
                      ))}
                  </div>
                </div>
                <hr />
                <div className="row" id="fs16">
                  <div className="col-4 text-secondary">Email</div>
                  <div className="col-8 text-secondary">{cntctDet.email}</div>
                </div>
                <div className="row" id="fs16">
                  <div className="col-4 text-secondary">Phone</div>
                  <div className="col-8 text-secondary">{cntctDet.mobile}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default Payment;
