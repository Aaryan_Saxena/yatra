import React, { Component } from "react";
import httpService from "./httpService";
import "./navbar.css";
import "./home.css";
import $ from "jquery";
import QueryPanal from "./queryPanal";
import queryString from "query-string";
import { Link } from "react-router-dom";
class ShowFlights extends Component {
  state = {
    flights: [],
    returnFlights: [],
    month: [
      "January",
      "Feberuary",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ],
    srchFlight: {},
    priceOpt: ["0-5000", "5000-10000", "10000-15000", "15000-20000"],
    timeOpt: ["0-6", "6-12", "12-18", "18-00"],
    airlineOpt: ["Go Air", "Indigo", "SpiceJet", "Air India"],
    aircraftOpt: [
      "Airbus A320 Neo",
      "AirbusA320",
      "Boeing737",
      "Airbus A320-100",
    ],
    queryPanal: false,
    sort: "",
    flightDet: -1,
    rtnflightDet: -1,
    goflightDet: -1,
    goTicket: {},
    returnTicket: {},
    view: 0,
  };
  async fetchData() {
    $("#query-panal").hide();
    let queryParams = queryString.parse(this.props.location.search);
    let searchStr = this.makeSearchString(queryParams);
    let srchFlight = await httpService.get(`/srchFlight`);
    let response = await httpService.get(
      `/flights/${srchFlight.data.from}/${srchFlight.data.to}?${searchStr}`
    );
    let returnFlights = await httpService.get(
      `/flights/${srchFlight.data.to}/${srchFlight.data.from}?${searchStr}`
    );
    console.log(response);
    this.setState({
      flights: response.data,
      srchFlight: srchFlight.data,
      returnFlights: returnFlights.data,
      goTicket: response.data[0],
      returnTicket: returnFlights.data[0],
    });
    $("#from").hover(function (e) {
      $("#from").toggleClass("hover");
    });
    $("#to").hover(function (e) {
      $("#to").toggleClass("hover");
    });
    for (let i = 0; i < response.data.length; i++) {
      $("#" + response.data[i].id).hover(function (e) {
        $("#" + response.data[i].id).toggleClass("boxShadow");
      });
      $("#f" + i).hide();
      $("#G" + i).hide();
    }
    for (let i = 0; i < returnFlights.data.length; i++) {
      $("#R" + i).hide();
      $("#" + returnFlights.data[i].id).hover(function (e) {
        $("#" + returnFlights.data[i].id).toggleClass("boxShadow");
      });
    }
  }
  componentDidMount() {
    this.fetchData();
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) this.fetchData();
  }
  handleFlightDetails = (index) => {
    console.log("#f" + index);
    let s1 = { ...this.state };
    s1.flightDet = s1.flightDet === index ? -1 : index;
    this.setState(s1);
    $("#f" + index).toggle();
  };
  handleFlightDetailsCls = (index) =>
    this.state.flightDet === index ? "fa-chevron-up" : "fa-chevron-down";
  handleReturnFlightDetails = (index) => {
    let s1 = { ...this.state };
    s1.rtnflightDet = s1.rtnflightDet === index ? -1 : index;
    $("#R" + index).toggle();
    this.setState(s1);
  };
  handleReturnFlightDetailsCls = (index) =>
    this.state.rtnflightDet === index ? "fa-chevron-up" : "fa-chevron-down";
  handleGoFlightDetails = (index) => {
    let s1 = { ...this.state };
    s1.goflightDet = s1.goflightDet === index ? -1 : index;
    $("#G" + index).toggle();
    this.setState(s1);
  };
  handleGoFlightDetailsCls = (index) =>
    this.state.goflightDet === index ? "fa-chevron-up" : "fa-chevron-down";
  handleQueryPanal = () => {
    $("#query-panal").toggle();
    let s1 = { ...this.state };
    s1.queryPanal = s1.queryPanal ? false : true;
    this.setState(s1);
  };
  handleShadow = (e) => {
    console.log(e.target.className);
  };
  handleFilter = () => {
    let json = {};
    json.price = "";
    json.time = "";
    json.flight = "";
    json.airbus = "";
    this.setState({ queryPanal: false });
    this.fetchData();
    this.callURL("/yatra/airSearch", json);
  };
  handleSort = (col) => {
    let s1 = { ...this.state };
    s1.sort = s1.sort === col ? "" : col;
    this.setState(s1);
    let queryParams = queryString.parse(this.props.location.search);
    queryParams.sort = queryParams.sort === col ? "" : col;
    this.fetchData();
    this.callURL(`/yatra/airSearch`, queryParams);
  };
  handleSortClass = (col) => (col === this.state.sort ? "text-primary" : "");
  handleOptionChange = (options) => {
    this.setState({ queryPanal: false });
    this.fetchData();
    this.callURL(`/yatra/airSearch`, options);
  };
  callURL = (url, options) => {
    let searchStr = this.makeSearchString(options);
    this.props.history.push({ pathname: url, search: searchStr });
  };
  makeSearchString = (options) => {
    let { price, time, flight, airbus, sort } = options;
    let searchStr = "";
    searchStr = this.addToQueryString(searchStr, "price", price);
    searchStr = this.addToQueryString(searchStr, "time", time);
    searchStr = this.addToQueryString(searchStr, "flight", flight);
    searchStr = this.addToQueryString(searchStr, "airbus", airbus);
    searchStr = this.addToQueryString(searchStr, "sort", sort);
    return searchStr;
  };
  addToQueryString = (str, paramName, paramValue) =>
    paramValue
      ? str
        ? `${str}&${paramName}=${paramValue}`
        : `${paramName}=${paramValue}`
      : str;
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    input.name === "goTicket"
      ? (s1[input.name] = s1.flights.find((fl) => fl.id === input.value))
      : (s1[input.name] = s1.returnFlights.find((fl) => fl.id === input.value));
    this.setState(s1);
  };
  makeGoTicket = () => this.setState({ goTicket: this.state.flights[0] });
  async handleBooking(id) {
    let response = await httpService.post(`/booking/${id}`);
  }
  async handleTwoWayBooking(goId, rtnId) {
    let response = await httpService.post(`/twoWayBooking/${goId}/${rtnId}`);
  }
  handleView = (view) => this.setState({ view: view });
  handleBorder = (view) => (this.state.view === view ? "border-bottom" : "");
  render() {
    let {
      srchFlight = {},
      flights = [],
      airlineOpt,
      aircraftOpt,
      priceOpt,
      timeOpt,
      queryPanal,
      returnFlights = [],
      sort,
      goTicket = {},
      returnTicket = {},
      view,
    } = this.state;
    let { tickets = {}, currentDate = {}, returnDate = {} } = srchFlight;
    let totTickets = 0;
    if (tickets.class) {
      totTickets =
        srchFlight.tickets.adult +
        srchFlight.tickets.child +
        srchFlight.tickets.infant;
    }
    let queryParams = queryString.parse(this.props.location.search);
    return (
      <div className="container-fluid" style={{ minWidth: "978px" }}>
        <div className="row mt-4 pt-2 pb-2 flight-banner " style={{}}>
          <div className="col-1 text-center mt-1">
            <i className="fa fa-fighter-jet fs-26"></i>
          </div>
          <div className="col-2">
            <div className="row" id="abc10">
              <div className="col">From</div>
            </div>
            <div className="row" id="from">
              <div className="col">
                <b>{srchFlight.from && srchFlight.from}</b>
              </div>
            </div>
          </div>
          <div className="col-1 mt-1 ">
            <i className="fa fa-exchange"></i>
          </div>
          <div className="col-2">
            <div className="row" id="abc10">
              <div className="col">To</div>
            </div>
            <div className="row" id="to">
              <div className="col">
                <b>{srchFlight.to && srchFlight.to}</b>
              </div>
            </div>
          </div>
          <div className="col-2">
            <div className="row banner-font">Date</div>
            <div className="row banner-font-value">
              <b>
                {currentDate.dd && currentDate.dd}{" "}
                {currentDate.mm && currentDate.mm}{" "}
                {currentDate.yy && currentDate.yy}
              </b>
            </div>
          </div>
          <div className="col-2">
            <div className="row banner-font">Traveller(s)</div>
            <div className="row banner-font-value">
              <b>
                {totTickets}, {tickets.class}
              </b>
            </div>
          </div>
          <div className="col-2 mt-1">
            <Link to="/yatra">
              <button className="btn btn-submit bold fs-14">
                Search Again
              </button>
            </Link>
          </div>
        </div>
        <div class="row pt-2 pb-2 box-shadow" id="id1">
          <div class="col-1 text-center fs-14">
            <i class="fa fa-filter"></i> Filter{" "}
          </div>
          {queryPanal ? (
            <React.Fragment>
              <div
                className="col-8 text-right"
                onClick={() => this.handleQueryPanal()}
              >
                <button className="btn btn-outline-secondary">Cancel</button>
              </div>
              <div className="col-2">
                {queryParams.price ||
                queryParams.time ||
                queryParams.flight ||
                queryParams.airbus ? (
                  <button
                    className="btn btn-danger py-1"
                    onClick={() => this.handleFilter()}
                  >
                    Clear Filter
                  </button>
                ) : (
                  ""
                )}
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <div
                class="col-2 text-center cursor-pointer"
                onClick={() => this.handleQueryPanal()}
              >
                Price <i class="fa fa-angle-down"></i>
              </div>
              <div
                class="col-2 text-center cursor-pointer"
                onClick={() => this.handleQueryPanal()}
              >
                Depart<i class="fa fa-angle-down"></i>
              </div>
              <div
                class="col-2 text-center cursor-pointer"
                onClick={() => this.handleQueryPanal()}
              >
                Airline <i class="fa fa-angle-down"></i>
              </div>
              <div
                class="col-2 text-center cursor-pointer"
                onClick={() => this.handleQueryPanal()}
              >
                Aircraft<i class="fa fa-angle-down"></i>
              </div>
              <div className="col-2">
                {queryParams.price ||
                queryParams.time ||
                queryParams.flight ||
                queryParams.airbus ? (
                  <button
                    className="btn btn-danger py-1"
                    onClick={() => this.handleFilter()}
                  >
                    Clear Filter
                  </button>
                ) : (
                  ""
                )}
              </div>
            </React.Fragment>
          )}
        </div>
        <div className="row bg-light expand" id="query-panal">
          <QueryPanal
            options={queryParams}
            airlineOpt={airlineOpt}
            priceOpt={priceOpt}
            timeOpt={timeOpt}
            aircraftOpt={aircraftOpt}
            onOptionChange={this.handleOptionChange}
          />
        </div>
        <br />
        <div className="row">
          <div className="col-9">
            <div className="row pl-4 pt-1 pb-1 mb-4 sort-box" id="id1">
              <div className="col-2">
                <strong>Sort By:</strong>
              </div>
              <div
                className={
                  "col-lg-2 col-3 text-right sort-option " +
                  this.handleSortClass("arrival")
                }
                onClick={() => this.handleSort("arrival")}
              >
                ARRIVE{" "}
                {sort === "arrival" && <i className="fa fa-arrow-up"></i>}
              </div>
              <div
                className={
                  "col-lg-2 col-3 text-right sort-option " +
                  this.handleSortClass("departure")
                }
                onClick={() => this.handleSort("departure")}
              >
                DEPART{" "}
                {sort === "departure" && <i className="fa fa-arrow-up"></i>}
              </div>
              <div
                className={
                  "col-lg-2 col-3 text-right sort-option " +
                  this.handleSortClass("price")
                }
                onClick={() => this.handleSort("price")}
              >
                PRICE {sort === "price" && <i className="fa fa-arrow-up"></i>}
              </div>
            </div>
            {returnDate.dd ? (
              <div className="row mb-5 pb-4">
                <div className="col-6">
                  <div className="row pl-4 pt-1 pb-1 mb-4 flight-box">
                    <div className="col-12 text-dark">
                      {srchFlight.from} <i className="fa fa-arrow-right"></i>{" "}
                      {srchFlight.to}
                    </div>
                  </div>
                  {flights.map((fl, index) => (
                    <div
                      className="row mt-1 ml-3 pt-2 box1 br-5"
                      key={index}
                      id={fl.id}
                    >
                      <div className="col">
                        <div className="row">
                          <div className="col-lg-2 col-3">
                            <img id="img1" src={fl.logo} />
                          </div>
                          <div className="col-lg-1 col-3 text-right fs9">
                            <div className="row">{fl.timeDept}</div>
                            <div
                              className="row"
                              style={{ fontSize: "x-small" }}
                            >
                              {fl.name}
                            </div>
                          </div>
                          <div className="col-lg-2 col-2">
                            <hr className="br-1" />
                          </div>
                          <div className="col-lg-1 col-3 text-left fs9">
                            <div className="row">{fl.timeArr}</div>
                          </div>
                          <div className="col-1 pt-1 d-none d-lg-block">
                            <span className="bl-3"></span>
                          </div>
                          <div className="col-lg-2 col-6 text-right fs8">
                            <div className="row">
                              <div className="col">{fl.total}</div>
                            </div>
                            <div className="row">
                              <div className="col">Non-Stop</div>
                            </div>
                          </div>
                          <div className="col-lg-2 col-3 text-right fs9">
                            ₹{fl.Price}
                          </div>
                          <div className="col-1 text-right">
                            <div className="row">
                              <div className="col text-right">
                                <input
                                  type="radio"
                                  name="goTicket"
                                  className="ng-pristine ng-valid ng-touched"
                                  value={fl.id}
                                  checked={goTicket.id && goTicket.id === fl.id}
                                  onChange={this.handleChange}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr className="p-0 m-0" />
                        <div className="row pb-1 pt-0">
                          <div className="col-7 d-none d-lg-block">
                            <a
                              className="fs-12 cursor-pointer text-blue text-decoration-none"
                              onClick={() => this.handleGoFlightDetails(index)}
                            >
                              Flight Details
                              <i
                                className={
                                  "fa " + this.handleGoFlightDetailsCls(index)
                                }
                              ></i>
                            </a>
                          </div>
                          <div className="col-5 text-right d-none d-lg-block">
                            <span id="rect"> eCash</span>
                            <span id="rect1"> &nbsp;₹ 250</span>
                          </div>
                        </div>
                        <div className="row flight-details" id={`G${index}`}>
                          <div className="col-7">
                            <div className="row header"> Flight Details </div>
                            <div className="row contents">
                              <div className="col-3">
                                <img src={fl.logo} className="w-40" />
                              </div>
                              <div className="col-6 text-left fs-13">
                                <div className="row">{fl.name}</div>
                                <div className="row fs-10 font-lightestgrey">
                                  {fl.airBus}
                                </div>
                              </div>
                              <div className="col-3 text-right">
                                <img
                                  src="https://i.ibb.co/31BTG9K/icons8-food-100.png"
                                  className="w-45"
                                />
                              </div>
                            </div>
                            <div className="row contents">
                              <div className="col-3 ml-1 fs-14">
                                <div className="row ml-1">{fl.desDept}</div>
                                <div className="row ml-1 fs-16 bold">
                                  <strong>{fl.timeDept}</strong>
                                </div>
                                <div className="row ml-1 fs-12">{fl.T1}</div>
                              </div>
                              <div className="col-5">
                                <div className="row fs-12">
                                  <div className="col-9 text-center">
                                    Time Taken
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-9">
                                    <hr className="hr-row" />
                                  </div>
                                  <div className="col-2 pt-2 font-lightestgrey fs-10">
                                    <svg
                                      aria-hidden="true"
                                      focusable="false"
                                      data-prefix="fas"
                                      data-icon="plane"
                                      className="svg-inline--fa fa-plane fa-w-18 "
                                      role="img"
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="0 0 576 512"
                                    >
                                      <path
                                        fill="currentColor"
                                        d="M480 192H365.71L260.61 8.06A16.014 16.014 0 0 0 246.71 0h-65.5c-10.63 0-18.3 10.17-15.38 20.39L214.86 192H112l-43.2-57.6c-3.02-4.03-7.77-6.4-12.8-6.4H16.01C5.6 128-2.04 137.78.49 147.88L32 256 .49 364.12C-2.04 374.22 5.6 384 16.01 384H56c5.04 0 9.78-2.37 12.8-6.4L112 320h102.86l-49.03 171.6c-2.92 10.22 4.75 20.4 15.38 20.4h65.5c5.74 0 11.04-3.08 13.89-8.06L365.71 320H480c35.35 0 96-28.65 96-64s-60.65-64-96-64z"
                                      ></path>
                                    </svg>
                                  </div>
                                </div>
                              </div>
                              <div className="col-3 fs-14">
                                <div className="row">{fl.desArr}</div>
                                <div className="row fs-16 bold">
                                  <strong>{fl.timeArr}</strong>
                                </div>
                                <div className="row fs-12">{fl.T2}</div>
                              </div>
                            </div>
                            <div className="row amen-details">
                              <div className="col text-center font-lightestgrey fs-12">
                                Checkin Baggage:
                                <i className="fa fa-briefcase font-lightestgrey fs-10"></i>{" "}
                                25kg
                              </div>
                            </div>
                          </div>
                          <div className="col-5 fare-details text-white">
                            <div className="row pl-1 pt-3 pb-3 fs-13 bold">
                              <div
                                className={
                                  "col-6 cursor-pointer " + this.handleBorder(0)
                                }
                                onClick={() => this.handleView(0)}
                              >
                                Fare Summary
                              </div>
                              <div
                                className={
                                  "col-6 cursor-pointer " + this.handleBorder(1)
                                }
                                onClick={() => this.handleView(1)}
                              >
                                Fare Rules
                              </div>
                            </div>
                            {view === 0 ? (
                              <React.Fragment>
                                <div className="row fs-13 bold">
                                  <div className="col-4">Fare Summary</div>
                                  <div className="col-4">Base and Fare</div>
                                  <div className="col-4">Fees and Taxes</div>
                                </div>
                                <div className="row fs-12 greyed">
                                  <div className="col-4">Adult X 1</div>
                                  <div className="col-4">₹ {fl.Price}</div>
                                  <div className="col-4">₹ 1000</div>
                                </div>
                                <hr />
                                <div className="row fs-14 bold">
                                  <div className="col-6">You Pay:</div>
                                  <div className="col-6 text-right">
                                    ₹ {fl.Price}
                                  </div>
                                </div>
                                <div className="row fs-12">
                                  <div className="col-12">
                                    {" "}
                                    Note: Total fare displayed above has been
                                    rounded off and may show a slight difference
                                    from actual fare.{" "}
                                  </div>
                                </div>
                              </React.Fragment>
                            ) : (
                              <React.Fragment>
                                <div className="row fs-13 bold">
                                  <div className="col-6">Duration</div>
                                  <div className="col-6 text-right">
                                    Per Passenger
                                  </div>
                                </div>
                                <div className="row fs-11 color-light">
                                  <div className="col-6">&gt;2 hours</div>
                                  <div className="col-6 text-right">₹ 2793</div>
                                </div>
                                <div className="row fs-11 color-light">
                                  <div className="col-12">
                                    We would recommend that you
                                    reschedule/cancel your tickets atleast 72
                                    hours prior to the flight departure
                                  </div>
                                </div>
                                <hr />
                                <div className="row fs-13 bold">
                                  <div className="col-12">
                                    Yatra Service Fee (YSF)**
                                    <div className="fs-12">
                                      (charged per passenger in addition to
                                      airline fee as applicable)
                                    </div>
                                  </div>
                                </div>
                                <div className="row color-light">
                                  <div className="col-6 fs-11">
                                    Online Cancellation Service Fee
                                  </div>
                                  <div className="col-6 text-right">₹ 400</div>
                                </div>
                                <div className="row color-light">
                                  <div className="col-6 fs-11">
                                    Offline Cancellation Service Fee
                                  </div>
                                  <div className="col-6 text-right">₹ 400</div>
                                </div>
                                <div className="row color-light">
                                  <div className="col-6 fs-11">
                                    Online Rescheduling Service Fee
                                  </div>
                                  <div className="col-6 text-right">₹ 400</div>
                                </div>
                                <div className="row fs-12">
                                  <div className="col-12">
                                    * Prior to the date/time of departure.
                                    **Please note: Yatra service fee is over and
                                    above the airline cancellation fee due to
                                    which refund type may vary.
                                  </div>
                                </div>
                              </React.Fragment>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
                <div className="col-6">
                  <div className="row pl-4 pt-1 pb-1 mb-4 flight-box">
                    <div className="col-12">
                      {srchFlight.to} <i className="fa fa-arrow-right"></i>{" "}
                      {srchFlight.from}
                    </div>
                  </div>
                  {returnFlights.map((fl, index) => (
                    <div
                      className="row mt-1 ml-3 pt-2 box1 br-5"
                      key={index}
                      id={fl.id}
                    >
                      <div className="col">
                        <div className="row">
                          <div className="col-lg-2 col-3">
                            <img id="img1" src={fl.logo} />
                          </div>
                          <div className="col-lg-1 col-3 text-right fs9">
                            <div className="row">{fl.timeDept}</div>
                            <div
                              className="row"
                              style={{ fontSize: "x-small" }}
                            >
                              {fl.name}
                            </div>
                          </div>
                          <div className="col-lg-2 col-2">
                            <hr style={{ border: "1px solid lightgrey" }} />
                          </div>
                          <div className="col-lg-1 col-3 text-left fs9">
                            <div className="row">{fl.timeArr}</div>
                          </div>
                          <div className="col-1 pt-1 d-none d-lg-block">
                            <span className="bl-3"></span>
                          </div>
                          <div className="col-lg-2 col-6 text-right fs8">
                            <div className="row">
                              <div className="col">{fl.total}</div>
                            </div>
                            <div className="row">
                              <div className="col">Non-Stop</div>
                            </div>
                          </div>
                          <div className="col-lg-2 col-3 text-right fs9">
                            ₹{fl.Price}
                          </div>
                          <div className="col-1 text-right">
                            <div className="row">
                              <div className="col text-right">
                                <input
                                  type="radio"
                                  name="returnTicket"
                                  className="ng-pristine ng-valid ng-touched"
                                  value={fl.id}
                                  checked={
                                    returnTicket.id && returnTicket.id === fl.id
                                  }
                                  onChange={this.handleChange}
                                />
                              </div>
                            </div>
                          </div>
                        </div>
                        <hr className="m-0 p-0" />
                        <div className="row pb-1 pt-0">
                          <div className="col-7 d-none d-lg-block">
                            <a
                              className="fs-12 text-blue cursor-pointer text-decoration-none"
                              onClick={() =>
                                this.handleReturnFlightDetails(index)
                              }
                            >
                              Flight Details
                              <i
                                className={
                                  "fa " +
                                  this.handleReturnFlightDetailsCls(index)
                                }
                              ></i>
                            </a>
                          </div>
                          <div className="col-5 text-right d-none d-lg-block">
                            <span id="rect"> eCash</span>
                            <span id="rect1"> &nbsp;₹ 250</span>
                          </div>
                        </div>
                        <div className="row flight-details" id={`R${index}`}>
                          <div className="col-7">
                            <div className="row header"> Flight Details </div>
                            <div className="row contents">
                              <div className="col-3">
                                <img src={fl.logo} className="w-40" />
                              </div>
                              <div className="col-6 text-left fs-13">
                                <div className="row">{fl.name}</div>
                                <div className="row fs-10 font-lightestgrey">
                                  {fl.airBus}
                                </div>
                              </div>
                              <div className="col-3 text-right">
                                <img
                                  src="https://i.ibb.co/31BTG9K/icons8-food-100.png"
                                  className="w-45"
                                />
                              </div>
                            </div>
                            <div className="row contents">
                              <div className="col-3 ml-1 fs-14">
                                <div className="row ml-1">{fl.desDept}</div>
                                <div className="row ml-1 fs-16 bold">
                                  <strong>{fl.timeDept}</strong>
                                </div>
                                <div className="row ml-1 fs-12">{fl.T1}</div>
                              </div>
                              <div className="col-5">
                                <div className="row fs-12">
                                  <div className="col-9 text-center">
                                    Time Taken
                                  </div>
                                </div>
                                <div className="row">
                                  <div className="col-9">
                                    <hr className="hr-row" />
                                  </div>
                                  <div className="col-2 pt-2 font-lightestgrey fs-10">
                                    <svg
                                      aria-hidden="true"
                                      focusable="false"
                                      data-prefix="fas"
                                      data-icon="plane"
                                      className="svg-inline--fa fa-plane fa-w-18 "
                                      role="img"
                                      xmlns="http://www.w3.org/2000/svg"
                                      viewBox="0 0 576 512"
                                    >
                                      <path
                                        fill="currentColor"
                                        d="M480 192H365.71L260.61 8.06A16.014 16.014 0 0 0 246.71 0h-65.5c-10.63 0-18.3 10.17-15.38 20.39L214.86 192H112l-43.2-57.6c-3.02-4.03-7.77-6.4-12.8-6.4H16.01C5.6 128-2.04 137.78.49 147.88L32 256 .49 364.12C-2.04 374.22 5.6 384 16.01 384H56c5.04 0 9.78-2.37 12.8-6.4L112 320h102.86l-49.03 171.6c-2.92 10.22 4.75 20.4 15.38 20.4h65.5c5.74 0 11.04-3.08 13.89-8.06L365.71 320H480c35.35 0 96-28.65 96-64s-60.65-64-96-64z"
                                      ></path>
                                    </svg>
                                  </div>
                                </div>
                              </div>
                              <div className="col-3 fs-14">
                                <div className="row">{fl.desArr}</div>
                                <div className="row fs-16 bold">
                                  <strong>{fl.timeArr}</strong>
                                </div>
                                <div className="row fs-12">{fl.T2}</div>
                              </div>
                            </div>
                            <div className="row amen-details">
                              <div className="col text-center font-lightestgrey fs-12">
                                Checkin Baggage:
                                <i className="fa fa-briefcase font-lightestgrey fs-10"></i>{" "}
                                25kg
                              </div>
                            </div>
                          </div>
                          <div className="col-5 text-white fare-details">
                            <div className="row pl-1 pt-3 pb-3 fs-13 bold">
                              <div
                                className={
                                  "col-6 cursor-pointer " + this.handleBorder(0)
                                }
                                onClick={() => this.handleView(0)}
                              >
                                Fare Summary
                              </div>
                              <div
                                className={
                                  "col-6 cursor-pointer " + this.handleBorder(1)
                                }
                                onClick={() => this.handleView(1)}
                              >
                                Fare Rules
                              </div>
                            </div>
                            {view === 0 ? (
                              <React.Fragment>
                                <div className="row fs-13 bold">
                                  <div className="col-4">Fare Summary</div>
                                  <div className="col-4">Base and Fare</div>
                                  <div className="col-4">Fees and Taxes</div>
                                </div>
                                <div className="row fs-12 greyed">
                                  <div className="col-4">Adult X 1</div>
                                  <div className="col-4">₹ {fl.Price}</div>
                                  <div className="col-4">₹ 1000</div>
                                </div>
                                <hr />
                                <div className="row fs-14 bold">
                                  <div className="col-6">You Pay:</div>
                                  <div className="col-6 text-right">
                                    ₹ {fl.Price}
                                  </div>
                                </div>
                                <div className="row fs-12">
                                  <div className="col-12">
                                    {" "}
                                    Note: Total fare displayed above has been
                                    rounded off and may show a slight difference
                                    from actual fare.{" "}
                                  </div>
                                </div>
                              </React.Fragment>
                            ) : (
                              <React.Fragment>
                                <div className="row fs-13 bold">
                                  <div className="col-6">Duration</div>
                                  <div className="col-6 text-right">
                                    Per Passenger
                                  </div>
                                </div>
                                <div className="row fs-11 color-light">
                                  <div className="col-6">&gt;2 hours</div>
                                  <div className="col-6 text-right">₹ 2793</div>
                                </div>
                                <div className="row fs-11 color-light">
                                  <div className="col-12">
                                    We would recommend that you
                                    reschedule/cancel your tickets atleast 72
                                    hours prior to the flight departure
                                  </div>
                                </div>
                                <hr />
                                <div className="row fs-13 bold">
                                  <div className="col-12">
                                    Yatra Service Fee (YSF)**
                                    <div className="fs-12">
                                      (charged per passenger in addition to
                                      airline fee as applicable)
                                    </div>
                                  </div>
                                </div>
                                <div className="row color-light">
                                  <div className="col-6 fs-11">
                                    Online Cancellation Service Fee
                                  </div>
                                  <div className="col-6 text-right">₹ 400</div>
                                </div>
                                <div className="row color-light">
                                  <div className="col-6 fs-11">
                                    Offline Cancellation Service Fee
                                  </div>
                                  <div className="col-6 text-right">₹ 400</div>
                                </div>
                                <div className="row color-light">
                                  <div className="col-6 fs-11">
                                    Online Rescheduling Service Fee
                                  </div>
                                  <div className="col-6 text-right">₹ 400</div>
                                </div>
                                <div className="row fs-12">
                                  <div className="col-12">
                                    * Prior to the date/time of departure.
                                    **Please note: Yatra service fee is over and
                                    above the airline cancellation fee due to
                                    which refund type may vary.
                                  </div>
                                </div>
                              </React.Fragment>
                            )}
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            ) : (
              <React.Fragment>
                {flights.map((fl, index) => (
                  <div
                    className="row mt-1 ml-3 pt-2 box1"
                    id={fl.id}
                    key={index}
                  >
                    <div className="col">
                      <div className="row deal">
                        <div className="col-1 deal-tag">DEAL</div>
                        <div className="col-10 content">
                          Book GoAir and get flat Rs 515 OFF per pax (upto Rs.
                          1,550). Use code: FLASHUPI
                        </div>
                      </div>
                      <div className="row mb-1 flight-det">
                        <div className="col-1">
                          <img src={fl.logo} className="flight-logo" />
                        </div>
                        <div className="col-1 p-0">
                          <div className="row p-0 flight-name">
                            <div className="col">{fl.name}</div>
                          </div>
                          <div className="row text-muted p-0 flight-code">
                            <div className="col">{fl.code}</div>
                          </div>
                        </div>
                        <div className="col-1 text-right flight-time">
                          <div className="row">{fl.timeDept}</div>
                          <div className="row" style={{ fontSize: "x-small" }}>
                            {fl.desDept}
                          </div>
                        </div>
                        <div className="col-1 pl-0">
                          <hr />
                        </div>
                        <div className="col-1 text-left flight-time">
                          <div className="row">{fl.timeArr}</div>
                          <div className="row" style={{ fontSize: "x-small" }}>
                            {fl.desArr}
                          </div>
                        </div>
                        <div className="col-1 pt-1">
                          <span className="bl-3"></span>
                        </div>
                        <div className="col-1 text-left flight-duration">
                          <div className="row">{fl.total}</div>
                          <div className="row">Non-Stop</div>
                        </div>
                        <div className="col-2"></div>
                        <div
                          className="col-2 p-0 text-right pr-3"
                          style={{ fontSize: "1.28571rem" }}
                        >
                          ₹ {fl.Price}
                        </div>
                        <div className="col-1 p-0">
                          <Link to="/booking">
                            <button
                              className="btn btn-sm btn-outline-danger text-danger"
                              id="book"
                              onClick={() => this.handleBooking(fl.id)}
                            >
                              Book
                            </button>
                          </Link>
                        </div>
                      </div>
                      <hr className="m-0 p-0" />
                      <div className="row pb-1">
                        <div className="col-7">
                          <a
                            className="yt-row-footer text-decoration-none"
                            id={`det${index}`}
                            onClick={() => this.handleFlightDetails(index)}
                          >
                            Flight Details
                            <i
                              className={
                                "fa " + this.handleFlightDetailsCls(index)
                              }
                            ></i>
                          </a>
                        </div>
                        <div className="col-5 text-right">
                          <span id="rect"> eCash</span>
                          <span id="rect1">₹ 250</span>
                        </div>
                      </div>
                      <div className="row flight-details" id={`f${index}`}>
                        <div className="col-7">
                          <div className="row header"> Flight Details </div>
                          <div className="row contents">
                            <div className="col-2">
                              <img src={fl.logo} className="w-40" />
                            </div>
                            <div className="col-7 text-left fs-13">
                              <div className="row">{fl.name}</div>
                              <div className="row fs-10 font-lightestgrey">
                                {" "}
                                {fl.airBus}
                              </div>
                            </div>
                            <div className="col-3 text-right">
                              <img
                                src="https://i.ibb.co/31BTG9K/icons8-food-100.png"
                                className="w-45"
                              />
                            </div>
                          </div>
                          <div className="row contents">
                            <div className="col-3 ml-1 fs-14">
                              <div className="row ml-1">{fl.desDept}</div>
                              <div className="row ml-1 fs-16 bold">
                                <strong>{fl.timeDept}</strong>
                              </div>
                              <div className="row ml-1 fs-12">{fl.T1}</div>
                            </div>
                            <div className="col-5">
                              <div className="row fs-12">
                                <div className="col-9 text-center">
                                  Time Taken
                                </div>
                              </div>
                              <div className="row">
                                <div className="col-9">
                                  <hr className="hr-row" />
                                </div>
                                <div className="col-2 pt-2 font-lightestgrey fs-10">
                                  <svg
                                    aria-hidden="true"
                                    focusable="false"
                                    data-prefix="fas"
                                    data-icon="plane"
                                    className="svg-inline--fa fa-plane fa-w-18 "
                                    role="img"
                                    xmlns="http://www.w3.org/2000/svg"
                                    viewBox="0 0 576 512"
                                  >
                                    <path
                                      fill="currentColor"
                                      d="M480 192H365.71L260.61 8.06A16.014 16.014 0 0 0 246.71 0h-65.5c-10.63 0-18.3 10.17-15.38 20.39L214.86 192H112l-43.2-57.6c-3.02-4.03-7.77-6.4-12.8-6.4H16.01C5.6 128-2.04 137.78.49 147.88L32 256 .49 364.12C-2.04 374.22 5.6 384 16.01 384H56c5.04 0 9.78-2.37 12.8-6.4L112 320h102.86l-49.03 171.6c-2.92 10.22 4.75 20.4 15.38 20.4h65.5c5.74 0 11.04-3.08 13.89-8.06L365.71 320H480c35.35 0 96-28.65 96-64s-60.65-64-96-64z"
                                    ></path>
                                  </svg>
                                </div>
                              </div>
                            </div>
                            <div className="col-3 fs-14">
                              <div className="row">{fl.desArr}</div>
                              <div className="row fs-16 bold">
                                <strong>{fl.timeArr}</strong>
                              </div>
                              <div className="row fs-12">{fl.T2}</div>
                            </div>
                          </div>
                          <div className="row amen-details">
                            <div className="col text-center font-lightestgrey fs-12">
                              Checkin Baggage:
                              <i className="fa fa-briefcase font-lightestgrey fs-10"></i>{" "}
                              25kg
                            </div>
                          </div>
                        </div>
                        <div className="col-5 fare-details text-white">
                          <div className="row pl-1 pt-3 pb-3 fs-13 bold">
                            <div
                              className={
                                "col-6 cursor-pointer " + this.handleBorder(0)
                              }
                              onClick={() => this.handleView(0)}
                            >
                              Fare Summary
                            </div>
                            <div
                              className={
                                "col-6 cursor-pointer " + this.handleBorder(1)
                              }
                              onClick={() => this.handleView(1)}
                            >
                              Fare Rules
                            </div>
                          </div>
                          {view === 0 ? (
                            <React.Fragment>
                              <div className="row fs-13 bold">
                                <div className="col-4">Fare Summary</div>
                                <div className="col-4">Base and Fare</div>
                                <div className="col-4">Fees and Taxes</div>
                              </div>
                              <div className="row fs-12 greyed">
                                <div className="col-4">Adult X 1</div>
                                <div className="col-4">₹ {fl.Price}</div>
                                <div className="col-4">₹ 1000</div>
                              </div>
                              <hr />
                              <div className="row fs-14 bold">
                                <div className="col-6">You Pay:</div>
                                <div className="col-6 text-right">
                                  ₹ {fl.Price}
                                </div>
                              </div>
                              <div className="row fs-12">
                                <div className="col-12">
                                  {" "}
                                  Note: Total fare displayed above has been
                                  rounded off and may show a slight difference
                                  from actual fare.{" "}
                                </div>
                              </div>
                            </React.Fragment>
                          ) : (
                            <React.Fragment>
                              <div className="row fs-13 bold">
                                <div className="col-6">Duration</div>
                                <div className="col-6 text-right">
                                  Per Passenger
                                </div>
                              </div>
                              <div className="row fs-11 color-light">
                                <div className="col-6">&gt;2 hours</div>
                                <div className="col-6 text-right">₹ 2793</div>
                              </div>
                              <div className="row fs-11 color-light">
                                <div className="col-12">
                                  We would recommend that you reschedule/cancel
                                  your tickets atleast 72 hours prior to the
                                  flight departure
                                </div>
                              </div>
                              <hr />
                              <div className="row fs-13 bold">
                                <div className="col-12">
                                  Yatra Service Fee (YSF)**
                                  <div className="fs-12">
                                    (charged per passenger in addition to
                                    airline fee as applicable)
                                  </div>
                                </div>
                              </div>
                              <div className="row color-light">
                                <div className="col-6 fs-11">
                                  Online Cancellation Service Fee
                                </div>
                                <div className="col-6 text-right">₹ 400</div>
                              </div>
                              <div className="row color-light">
                                <div className="col-6 fs-11">
                                  Offline Cancellation Service Fee
                                </div>
                                <div className="col-6 text-right">₹ 400</div>
                              </div>
                              <div className="row color-light">
                                <div className="col-6 fs-11">
                                  Online Rescheduling Service Fee
                                </div>
                                <div className="col-6 text-right">₹ 400</div>
                              </div>
                              <div className="row fs-12">
                                <div className="col-12">
                                  * Prior to the date/time of departure.
                                  **Please note: Yatra service fee is over and
                                  above the airline cancellation fee due to
                                  which refund type may vary.
                                </div>
                              </div>
                            </React.Fragment>
                          )}
                          <div className="row pt-2 pb-2">
                            <div className="col-12 text-center">
                              <Link to="/booking">
                                <button
                                  className="btn btn-danger text-white btn-block"
                                  routerlinkactive="router-link-active"
                                  tabindex="0"
                                  onClick={() => this.handleBooking(fl.id)}
                                >
                                  Book
                                </button>
                              </Link>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <br />
                  </div>
                ))}
              </React.Fragment>
            )}
            {returnDate.dd && (
              <div
                className="row fixed-bottom pt-4 pb-4 text-white"
                style={{ backgroundColor: "rgb(67, 38, 78)" }}
              >
                <div className="col-4 d-none d-lg-block">
                  <div className="row">
                    <div className="col-3 text-center">
                      <img src={goTicket.logo} className="ticket-logo" />
                    </div>
                    <div
                      className="col-2 d-none d-lg-block"
                      style={{ fontSize: "0.85714rem" }}
                    >
                      {goTicket.name}
                    </div>
                    <div className="col-2 d-none d-lg-block">
                      {goTicket.timeDept}
                    </div>
                    <div class="col-3 d-none d-lg-block ">
                      <hr className="border-white" />
                    </div>
                    <div className="col-2 d-none d-lg-block">
                      {goTicket.timeArr}
                    </div>
                  </div>
                </div>
                <div className="col-4 d-none d-lg-block">
                  <div className="row">
                    <div className="col-3 text-center">
                      <img src={returnTicket.logo} className="ticket-logo" />
                    </div>
                    <div className="col-2" style={{ fontSize: "0.85714rem" }}>
                      {returnTicket.name}
                    </div>
                    <div className="col-2">{returnTicket.timeDept}</div>
                    <div className="col-3">
                      <hr className="border-white" />
                    </div>
                    <div className="col-2">{returnTicket.timeArr}</div>
                  </div>
                </div>
                <div className="col-lg-4 col-12">
                  <div className="row">
                    <div className="col-6 ml-lg-0 ml-1">
                      <div className="row" style={{ fontSize: "0.85714rem" }}>
                        <div className="col">Total Fare</div>
                      </div>
                      <div className="row" style={{ fontSize: "1.57143rem" }}>
                        <div className="col">
                          <strong>
                            ₹{goTicket.Price + returnTicket.Price}
                          </strong>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-6 col-4 mt-1">
                      <Link to="/booking">
                        <button
                          className="btn btn-danger text-white"
                          onClick={() =>
                            this.handleTwoWayBooking(
                              goTicket.id,
                              returnTicket.id
                            )
                          }
                        >
                          {" "}
                          Book{" "}
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            )}
          </div>
          <div className="col-2 ml-1">
            <img
              src="https://i.ibb.co/18cngjz/banner-1575268481164-ICICI-Dom-Flight-New-Homepage-SRP-182-X300.png"
              alt=""
              style={{ width: "100%" }}
            />
          </div>
        </div>
      </div>
    );
  }
}
export default ShowFlights;
