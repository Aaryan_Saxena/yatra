import React, { Component } from "react";
import "./navbar.css";
import "./home.css";
import $ from "jquery";
import { Link } from "react-router-dom";
class LeftPanel extends Component {
  componentDidMount() {
    let { option } = this.props;
    option === "Your eCash" ? $("#eCashDD").show() : $("#eCashDD").hide();
    $("#ecash").click(function (e) {
      $("#eCashDD").toggle();
    });
    $("#summary").hover(function (e) {
      $("#summary").toggleClass("eCashHover");
    });
    $("#transfer").hover(function (e) {
      $("#transfer").toggleClass("eCashHover");
    });
    $("#convertCoupons").hover(function (e) {
      $("#convertCoupons").toggleClass("eCashHover");
    });
    $("#coupons").hover(function (e) {
      $("#coupons").toggleClass("eCashHover");
    });
  }
  handleClass = (option) =>
    this.props.option === option ? "row-tab-selected" : "row-tab";
  render() {
    return (
      <div className="col-3 leftpanel-box">
        <div className="leftpanel">
          <Link to="/yatra/allBookings" className="text-decoration-none">
            <div className={"" + this.handleClass("My Bookings")}>
              <div className="mt-3 mb-3 ml-3">
                {" "}
                <i className="fa fa-plane fs15"></i>
                &nbsp;&nbsp; <span class="fs-sm">ALL BOOKINGS</span>
              </div>
            </div>
          </Link>
          <div className="row-tab">
            <div className="mt-3 mb-3 ml-3">
              {" "}
              <i className="fas fa-edit fs15"></i>
              &nbsp;&nbsp; MODIFY BOOKINGS
            </div>
          </div>
          <div className="row-tab">
            <div className="mt-3 mb-3 ml-3">
              {" "}
              <i className="fas fa-ticket-alt fs15"></i>
              &nbsp;&nbsp; TICKETS/VOUCHERS
            </div>
          </div>
          <div className="row-tab">
            <div className="mt-3 mb-3 ml-3">
              {" "}
              <i className="fa fa-rupee-sign fs15"></i>
              &nbsp;&nbsp; CLAIM REFUND
            </div>
          </div>
          <div className="row-tab">
            <div className="mt-3 mb-3 ml-3">
              {" "}
              <i className="fas fa-file-alt fs15"></i>
              &nbsp;&nbsp; FLIGHT REFUND STATUS
            </div>
          </div>
          <Link to="/yatra/eCash" className="text-decoration-none">
            <div className={"" + this.handleClass("Your eCash")} id="ecash">
              <div className="mt-3 mb-3 ml-3">
                {" "}
                <i className="fas fa-wallet fs15"></i>
                &nbsp;&nbsp;ECASH
                <i className="fa fa-chevron-down eCash-chevron"></i>
              </div>
            </div>
          </Link>
          <div id="eCashDD">
            <div className="border bg-light cursor-pointer" id="summary">
              <div className="mt-3 mb-3 ml-3" id="abc">
                {" "}
                <i className="fa fa-chevron-left fs-10"></i>
                &nbsp;&nbsp; Summary
              </div>
            </div>
            <div className="border bg-light cursor-pointer" id="transfer">
              <div className="mt-3 mb-3 ml-3" id="abc">
                {" "}
                <i className="fa fa-chevron-left fs-10"></i>
                &nbsp;&nbsp; Transfer
              </div>
            </div>
            <div className="border bg-light cursor-pointer" id="convertCoupons">
              <div className="mt-3 mb-3 ml-3" id="abc">
                {" "}
                <i className="fa fa-chevron-left fs-10"></i>
                &nbsp;&nbsp; Convert To Coupons
              </div>
            </div>
            <div className="border bg-light cursor-pointer" id="coupons">
              <div className="mt-3 mb-3 ml-3" id="abc">
                {" "}
                <i className="fa fa-chevron-left fs-10"></i>
                &nbsp;&nbsp; My Coupons
              </div>
            </div>
          </div>
          <Link to="/yatra/profile" className="text-decoration-none">
            <div className={"" + this.handleClass("Your Profile")}>
              <div className="mt-3 mb-3 ml-3">
                {" "}
                <i className="fa fa-user fs15"></i>
                &nbsp;&nbsp; YOUR PROFILE
              </div>
            </div>
          </Link>
          <div className="row-tab">
            <div className="mt-3 mb-3 ml-3">
              {" "}
              <i className="fas fa-pen fs15"></i>
              &nbsp;&nbsp; YOUR COMMUNICATION
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default LeftPanel;
