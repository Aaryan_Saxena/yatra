import React, { Component } from "react";
import $ from "jquery";
import { Link } from "react-router-dom";
import "./navbar.css";
import "./home.css";
class ShowNavbar extends Component {
  componentDidMount() {
    $("#user").hide();
    $("#userdd").hover(function () {
      $("#user").toggle();
    });
    $("#content").hide();
    $("#onhover").hover(function () {
      $("#content").toggle();
    });
    $("#search-content").hide();
    $("#recent-search").hover(function () {
      $("#search-content").toggle();
    });
    $("#plane").hover(function (e) {
      $("#plane").toggleClass("hover");
    });
    $("#bed").hover(function (e) {
      $("#bed").toggleClass("hover");
    });
    $("#bus").hover(function (e) {
      $("#bus").toggleClass("hover");
    });
    $("#taxi").hover(function (e) {
      $("#taxi").toggleClass("hover");
    });
    $("#allBookings").hover(function (e) {
      if (e.target.className.match("profileHover")) {
        $("#allBookings").removeClass("profileHover");
      } else {
        $("#allBookings").addClass("profileHover");
      }
    });
    $("#eCash").hover(function (e) {
      if (e.target.className.match("profileHover")) {
        $("#eCash").removeClass("profileHover");
      } else {
        $("#eCash").addClass("profileHover");
      }
    });
    $("#profile").hover(function (e) {
      if (e.target.className.match("profileHover")) {
        $("#profile").removeClass("profileHover");
      } else {
        $("#profile").addClass("profileHover");
      }
    });
    $("#logout").hover(function (e) {
      if (e.target.className.match("profileHover")) {
        $("#logout").removeClass("profileHover");
      } else {
        $("#logout").addClass("profileHover");
      }
    });
  }
  render() {
    let { user = {} } = this.props;
    return (
      <div className="container-fluid">
        <div className="row navbar pb-0 pl-0 pt-3">
          <div className="col-1" style={{ minWidth: "100px" }}>
            <Link to="/yatra">
              <img
                src="https://www.yatra.com/content/fresco/beetle/images/newIcons/yatra_logo.svg"
                alt=""
                className="brand"
              />
            </Link>
          </div>
          <div className="col-1 ml-2 mr-2 icon-header" id="plane">
            <div className="row pl-3 mb-1">
              <i className="fas fa-plane"></i>
            </div>
            <div className="row pl-1">Flights</div>
          </div>
          <div className="col-1 mx-2 icon-header" id="bed">
            <div className="row pl-2 mb-1">
              <i className="fas fa-bed"></i>
            </div>
            <div className="row">Hotels</div>
          </div>
          <div className="col-1 mx-2 icon-header" id="bus">
            <div className="row pl-2 mb-1">
              <i className="fas fa-bus"></i>
            </div>
            <div className="row">Buses</div>
          </div>
          <div className="col-1 mx-2 icon-header" id="taxi">
            <div className="row pl-2 mb-1">
              <i className="fas fa-taxi"></i>
            </div>
            <div className="row">Cabs</div>
          </div>
          <div className="col-1"></div>
          {user && user.success ? (
            <div className="dropdownLogin" id="userdd">
              <div className="dropbtn1">
                <span>
                  Hi {user.fname}
                  <i className="fa fa-chevron-down ml-1 fs-10"></i>
                </span>
              </div>
              <div className="row hoverLogin fs-14 cursor-pointer" id="user">
                <Link to="/yatra/allBookings" className="text-decoration-none">
                  <div className="col-12 py-2 text-dark p-0" id="allBookings">
                    <span className="px-2 fs-10 text-grey">
                      <i className="fa fa-chevron-left"></i>
                    </span>
                    My Booking
                  </div>
                </Link>
                <Link to="/yatra/eCash" className="text-decoration-none">
                  <div className="col-12 py-2 text-dark p-0" id="eCash">
                    <span className="px-2 fs-10 text-grey">
                      <i className="fa fa-chevron-left"></i>
                    </span>{" "}
                    My eCash
                  </div>
                </Link>
                <Link to="/yatra/profile" className="text-decoration-none">
                  <div className="col-12 py-2 text-dark p-0" id="profile">
                    <span className="px-2 fs-10 text-grey">
                      <i className="fa fa-chevron-left"></i>
                    </span>
                    My Profile
                  </div>
                </Link>
                <Link to="/logout" className="text-decoration-none">
                  <div className="col-12 py-2 text-dark px-4" id="logout">
                    Logout
                  </div>
                </Link>
              </div>
            </div>
          ) : (
            <div className="col-1 text-right p-0" style={{ minWidth: "150px" }}>
              <div className="mt-1 p-0">
                <div className="dropdown" id="onhover">
                  <div className="dropbtn1">
                    {" "}
                    My Account
                    <i className="fa fa-chevron-down fs-10"></i>
                  </div>
                  <div className="dropdown-content" id="content">
                    <div className="row mt-2">
                      <div className="col-3 ml-2">
                        <i className="fa fa-user-circle fs-35 text-grey"></i>
                      </div>
                      <div className=" text-left">
                        <span className="yt-header-drop-down-text">
                          My Bookings
                        </span>
                        <div>
                          <span className="yt-header-drop-down-text">
                            My eCash
                          </span>
                        </div>
                      </div>
                    </div>
                    <div className="row mt-2 mb-4">
                      <div className="col-5">
                        <Link to="/common">
                          <button
                            type="button"
                            className="yt-header-drop-down-text pgLogIn"
                          >
                            &nbsp; Login &nbsp;
                          </button>
                        </Link>
                      </div>
                      <div className="col-5">
                        <Link to="/common">
                          <button
                            type="button"
                            className="yt-header-drop-down-text pgSignUp"
                          >
                            Sign Up
                          </button>
                        </Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
          <div className="col-1 p-0 text-right" style={{ minWidth: "100px" }}>
            <div className="mt-1 ml-1">
              <div className="dropdown">
                <div className="dropbtn1">
                  Support
                  <i className="fa fa-chevron-down fs-10"></i>
                </div>
              </div>
            </div>
          </div>
          <div
            className="col-2 m-0 p-0 text-left"
            style={{ maxWidth: "150px" }}
          >
            <div className="mt-1">
              <div className="dropdown" id="recent-search">
                <div className="dropbtn1">
                  Recent Search
                  <i className="fa fa-chevron-down fs-10" yy></i>
                </div>
                <div className="dropdown-content" id="search-content">
                  <div className="col-12 mt-2 text-left">
                    Things you view while searching are saved here.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="mt-1 col-1 mr-4 p-0 text-left">Offers</div>
        </div>
      </div>
    );
  }
}
export default ShowNavbar;
