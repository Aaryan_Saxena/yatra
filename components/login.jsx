import React, { Component } from "react";
import authService from "./authService";
import httpService from "./httpService";
import "./home.css";
class Login extends Component {
  state = {
    user: { email: "", password: "" },
    view: 0,
    errors: {},
  };
  handleChange = (e) => {
    let s1 = { ...this.state };
    let { currentTarget: input } = e;
    s1.user[input.name] = input.value;
    this.setState(s1);
  };
  handleSubmitEmail = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    if (!s1.user.email) {
      s1.errors.email = "Please enter your Email Id / Mobile Number";
    } else {
      s1.view = 1;
    }
    this.setState(s1);
  };
  handleLogin = (e) => {
    e.preventDefault();
    let s1 = { ...this.state };
    if (!s1.user.password) {
      s1.errors.password = "Please enter your password";
    } else {
      this.postData("/login", this.state.user);
    }
    this.setState(s1);
  };
  async postData(url, obj) {
    try {
      let response = await httpService.post(url, obj);
      console.log(response);
      authService.login(response.data);
      window.location = "/yatra";
    } catch (e) {
      alert("Invalid Email or Password");
      this.setState({ view: 0, user: { email: "", password: "" }, errors: {} });
    }
  }
  handleView = (view) => {
    let s1 = { ...this.state };
    s1.user = { email: "", password: "" };
    s1.errors = {};
    s1.view = view;
    this.setState(s1);
  };
  render() {
    let { email, password } = this.state.user;
    let { view, errors = {} } = this.state;
    return (
      <div className="container-fluid">
        <div className="row mt-4">
          <div className="col-12 text-center">
            <h2>Welcome to Yatra!</h2>
            <div>Please Login/Register using your Email/Mobile to continue</div>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-3"></div>
          <div className="col-6">
            <div className="row bg-light">
              {view === 0 ? (
                <div className="col-6 text-center mt-4">
                  <i className="fa fa-user-circle user-circle"></i>
                  <div className="mt-2">EMAIL ID / MOBILE NUMBER</div>
                  <div className="mt-2">
                    <input
                      className="form-control"
                      placeholder="EMAIL ID / MOBILE NUMBER"
                      name="email"
                      type="text"
                      value={email}
                      onChange={this.handleChange}
                    />
                    <span className="text-danger">{errors.email}</span>
                  </div>
                  <div className="mt-4">
                    <button
                      className="form-control btn btn-danger"
                      type="submit"
                      onClick={this.handleSubmitEmail}
                    >
                      Continue
                    </button>
                  </div>
                  <div className="text-center mt-3">
                    By proceeding, you agree with our Terms of Service, Privacy
                    Policy &amp; Master User Agreement.
                  </div>
                  <hr className="mb-2" />
                </div>
              ) : (
                <div className="col-6 text-center mt-4">
                  <i className="fa fa-user-circle user-circle"></i>
                  <div className="mt-2">{email}</div>
                  <div className="mt-2">
                    <input
                      className="form-control"
                      placeholder="Enter Your Password"
                      name="password"
                      type="password"
                      value={password}
                      onChange={this.handleChange}
                    />
                    <span className="text-danger">{errors.password}</span>
                  </div>
                  <div class="text-center mt-3 text-primary cursor-pointer">
                    OR Login using OTP
                  </div>
                  <div className="mt-4">
                    <button
                      className="form-control btn btn-danger"
                      type="submit"
                      onClick={this.handleLogin}
                    >
                      Login
                    </button>
                  </div>
                  <div
                    class="text-center mt-3 text-primary cursor-pointer"
                    onClick={() => this.handleView(0)}
                  >
                    Login as a different user
                  </div>
                </div>
              )}
              <div className="col-6 text-center mt-4">
                <div className="loginDet">
                  Logged In/Registered users get MORE!
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-calendar login-infoColor"
                    aria-hidden="true"
                  ></i>{" "}
                  View/ Cancel/ Reschedule bookings
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-ticket login-infoColor"
                    aria-hidden="true"
                  ></i>{" "}
                  Check booking history, manage cancellations &amp; print
                  eTickets
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-edit login-infoColor"
                    aria-hidden="true"
                  ></i>{" "}
                  Book faster with Pre-Filled Forms, saved Travellers &amp;
                  Saved Cards
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-money-bill login-infoColor"
                    aria-hidden="true"
                  ></i>{" "}
                  Use Yatra eCash to get discounts
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-barcode login-infoColor"
                    aria-hidden="true"
                  ></i>{" "}
                  Transfer eCash to your Family/Friends
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-desktop login-infoColor"
                    aria-hidden="true"
                  ></i>
                  Convert eCash to Shopping Coupons from Amazon, BookMyShow,
                  etc.
                </div>
                <div className="inner-loginDet">
                  <i
                    className="fa fa-briefcase login-infoColor"
                    aria-hidden="true"
                  ></i>{" "}
                  Do you have GST number?Additional Benefits of Free Meals, Low
                  Cancellation Fee, Free Rescheduling for SME business customers
                </div>
                <br /> <br />
              </div>
            </div>
          </div>
          <div className="col-3"></div>
        </div>
      </div>
    );
  }
}
export default Login;
